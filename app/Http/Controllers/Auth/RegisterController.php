<?php

namespace App\Http\Controllers\Auth;

use App\Mail\ConfirmAccount;
use App\Models\User;
use App\Models\Invitee;
use App\Libraries\Misc;
use App\Models\Certificate;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Auth\Events\Registered;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/check-email';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

	/**
	 * @param Request $request
	 *
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function showRegistrationForm(Request $request)
    {
	    $referrer = null;
    	if ($request->has('ref')) {
		    $referrer = Certificate::where('code', $request->get('ref'))->first();

		    if ($referrer && $referrer->isTaken()) {
		    	$referrer = $referrer->owner->availableCertificate();
		    }

		    //dd($referrer);
	    }

	    $data['referrer'] = $referrer;
	    return view('auth.register', $data);
    }
	
	/**
	 * Handle a registration request for the application.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function register(Request $request)
	{
		$this->validator($request->all())->validate();
		
		event(new Registered($user = $this->create($request->all())));
		
		//$this->guard()->login($user);
		Mail::to($user)->send(new ConfirmAccount($user));
		
		return $this->registered($request, $user)
			?: redirect($this->redirectPath());
	}

	/**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
    	$rules = [
		    'name' => 'required|max:255',
		    'phone_number' => 'required',
		    'email' => 'required|email|max:255|unique:users',
		    'password' => 'required|min:6|confirmed',
		    'terms' => 'accepted'
	    ];

    	if (!isset($data['ref_cert_id'])) {
    		$rules['currency'] = 'required';
    		$rules['amount'] = 'required';
	    }

        return Validator::make($data, $rules);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
    	$user = User::create([
    		'name' => $data['name'],
		    'email' => $data['email'],
		    'password' => bcrypt($data['password']),
		    'phone' => $data['h_phone'],
		    'code' => Misc::generateUniqueRefID(),
		    'ref_cert_id' => isset($data['ref_cert_id']) ? $data['ref_cert_id'] : null
	    ]);

    	if ($user) {
		    if (isset($data['inv'])) {
		    	$invitee = Invitee::where('invite_code', $data['inv'])->first();
			    
		    	if ($invitee)
		    		$invitee->markAsJoined();
	        }
	        
    		if ($user->hasReferrer()) {
    			$referrerPackage = $user->referrerCertificate->owner->package;
			    $packageData = [
				    'currency' => $referrerPackage->currency,
				    'amount' => $referrerPackage->amount
			    ];
			    $user->referrerCertificate->assignTo($user->id);
			    $user->referrerCertificate->promote();
		    } else {
			    $packageData = [
				    'currency' => $data['currency'],
				    'amount' => $data['amount']
			    ];
		    }

    		$package = $user->package()->create($packageData);
		    
		    if ($packageData['currency']=="NGN" && (int)$packageData['amount']<5000) {
		    	$packageData['amount'] = $packageData['amount'] * 3;
		    }

    		$reg_fee = $user->registrationFee()->create($packageData);

    		if($package && $reg_fee) {
			    return $user;
		    }
		    // rollback user creation if package or reg fee isn't created
		    $user->delete();
	    }

	    return redirect()->back()->with('status', 'Unable to complete registration. Please try again');
    }

	public function reactivateUserForm( User $user ) {

    }
	
	public function checkEmail () {
		return view('auth.checkemail');
    }
}
