<?php

namespace App\Http\Controllers;

use App\Models\Testimonial;
use Illuminate\Http\Request;

class WelcomeController extends Controller
{
	public function index() {
	    $testimonials = Testimonial::enabled()->latest()->take(5)->get();
		return view('index', compact('testimonials'));
    }

	public function about() {
		return view('about');
    }
}
