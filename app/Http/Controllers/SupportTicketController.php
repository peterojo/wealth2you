<?php

namespace App\Http\Controllers;

use App\Libraries\Misc;
use App\Models\Contribution;
use App\Models\SupportTicket;
use Illuminate\Http\Request;

class SupportTicketController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
	        'subject' => 'required',
	        'message' => 'required'
        ]);

        if ($request->has('trans_id')) {
	        $contribution = Contribution::where('trans_id', $request->trans_id)->first();
	        $this->authorize('createTicket', $contribution);

	        $ticket = $contribution->tickets()->where('user_id', auth()->id())->first() ?:
		        $contribution->tickets()->create([
			        'user_id' => auth()->id(),
			        'ticket_code' => Misc::generateTicketCode(),
			        'subject' => $request->subject
		        ]);
        } else {
        	$ticket = SupportTicket::create([
		        'user_id' => auth()->id(),
		        'ticket_code' => Misc::generateTicketCode(),
		        'subject' => $request->subject
	        ]);
        }
	    $ticket->messages()->create([
		    'user_id' => auth()->id(),
		    'message' => $request->message
	    ]);

        return redirect()->back()
	        ->with('message', 'Thanks for contacting us. Your message has been received')
	        ->with('type', 'success');
    }

	public function storeTicketMessage( Request $request, SupportTicket $ticket ) {
		$this->authorize('addMessage', $ticket);

		$this->validate($request, ['message' => 'required|min:8'], [
			'message.min' => 'Your message is too short'
		]);

		$ticket->messages()->create([
			'user_id' => auth()->id(),
			'message' => $request->message
		]);

		return redirect()->back()
		                 ->with('message', 'Your message has been received')
		                 ->with('type', 'success');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\SupportTicket  $ticket
     *
*@return \Illuminate\Http\Response
     */
    public function show(SupportTicket $ticket)
    {
    	if ( !$ticket->exists )
    		$ticket = auth()->user()->latestTicket();

    	$my_tickets = auth()->user()->tickets->reverse();

        return view('dashboard.support', compact('my_tickets', 'ticket'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\SupportTicket  $supportTicket
     * @return \Illuminate\Http\Response
     */
    public function edit(SupportTicket $supportTicket)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\SupportTicket  $supportTicket
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SupportTicket $supportTicket)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\SupportTicket  $supportTicket
     * @return \Illuminate\Http\Response
     */
    public function destroy(SupportTicket $supportTicket)
    {
        //
    }
}
