<?php

namespace App\Http\Controllers;

use App\Services\SMS;

class SMSController extends Controller
{
    public function send()
    {
        $message="Hello Peter from Wealth2You!";
        $recipient = "+2348024990162";

        $sms = new SMS;

        $sms->send($recipient, $message);
	}
}