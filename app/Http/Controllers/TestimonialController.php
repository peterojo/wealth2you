<?php

namespace App\Http\Controllers;

use App\Models\Testimonial;
use App\Http\Requests\CreateTestimonialRequest;
use App\Http\Requests\UpdateTestimonialRequest;

class TestimonialController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $testimonials = Testimonial::latest()->paginate(20);

        return view('admin.testimonials', compact('testimonials'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $testimonials = auth()->user()->testimonials;
        return view('dashboard.testimonials', compact('testimonials'));
    }

    /**
     * Store a newly created testimonial in database.
     *
     * @param  \App\Http\Requests\CreateTestimonialRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateTestimonialRequest $request)
    {
        auth()->user()->testimonials()->create([
            'title' => $request->title,
            'body'  => $request->body
        ]);

        return redirect()->back()
                ->with('message', 'Testimonial created successfully.')
                ->with('type', 'success');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Testimonial  $testimonial
     * @return \Illuminate\Http\Response
     */
    public function show(Testimonial $testimonial)
    {
        return response()->json([
            'data'  => $testimonial->toArray()
        ], 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Testimonial  $testimonial
     * @return \Illuminate\Http\Response
     */
    public function edit(Testimonial $testimonial)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateTestimonialRequest $request
     * @param  \App\Models\Testimonial $testimonial
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateTestimonialRequest $request, Testimonial $testimonial)
    {
        $this->authorize('update', $testimonial);

        $testimonial->update([
            'title' => $request->title,
            'body'  => $request->body
        ]);

        return response()->json([
            'message'   => 'Testimony updated successfully.',
            'data'      => $testimonial->toArray()
        ], 201);
    }

    /**
     * Remove the specified testimonial from database.
     *
     * @param  \App\Models\Testimonial  $testimonial
     * @return \Illuminate\Http\Response
     */
    public function destroy(Testimonial $testimonial)
    {
        $this->authorize('delete', $testimonial);

        $testimonial->delete();

        return response()->json([], 204);
    }

    public function enable(Testimonial $testimonial)
    {
        $this->authorize('disable', $testimonial);

        $testimonial->enable();

        return back()->with('message', 'Testimonial enabled.')->with('type', 'success');
    }

    public function disable(Testimonial $testimonial)
    {
        $this->authorize('disable', $testimonial);

        $testimonial->disable();

        return back()->with('message', 'Testimonial disabled.')->with('type', 'success');
    }
}
