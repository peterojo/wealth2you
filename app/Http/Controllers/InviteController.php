<?php

namespace App\Http\Controllers;

use App\Mail\SendInvite;
use App\Models\Invitee;
use App\Services\SMS;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;

class InviteController extends Controller
{
	public function __construct() {
		$this->middleware(['has-certificates'])->except('show');
	}

	public function index() {
		return view('invite.index');
    }

	public function store( Request $request ) {
		$this->validate($request, [
			'name.*' => 'required_with:phone.*,email.*',
			'email.*' => 'required|email|unique:users,email|unique:invitees,email'
		], [
			'name.*.required_with' => 'You entered an email/phone number without a name',
			'email.*.email' => 'You entered an invalid email address',
			'email.*.unique' => 'You entered an email address that already exists in our records or has already been invited.'
		]);

		$entries = [];
		for ($i = 0; $i < count($request->email); $i++) {
			$entries[] = [
				'name' => $request->name[$i],
				'email' => $request->email[$i],
				'phone' =>$request->phone[$i],
				'invite_code' => str_random(32).time()
			];
		}

		auth()->user()->invitees()->createMany($entries);

		$smsMessage = "Hello! ".auth()->user()->name." has invited you to join him on Wealth2You.\n
		               Click the following link to join. ".route('register')."?ref=".auth()->user()->certificates[0]->code;
        (new SMS)->send($request->phone, $smsMessage);
		
		$emailData = [];
		foreach ($entries as $entry) {
			$emailData['name'] = $entry['name'];
			$emailData['sender'] = auth()->user()->name;
			$emailData['emailBody'] = $request->has('emailBody')?$request->get('emailBody'):"Join me at Wealth 2 You today and take control of your finances.";
			$emailData['link'] = route('invite.show', $entry['invite_code']);

			Mail::to($entry['email'])->queue(new SendInvite($emailData));
		}

		return back()->with('status', 'Thanks for inviting your contacts!')
			->with('type', 'success');
	}
	
	public function show ( Invitee $invitee ) {
		$certificate = $invitee->inviter->availableCertificate();
		
		return redirect(route('register')."?ref=".$certificate->code."&inv=".$invitee->invite_code);
	}
}
