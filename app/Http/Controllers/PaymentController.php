<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Contribution;
use Ixudra\Curl\Facades\Curl;
use App\Jobs\NotifyUserDenied;
use App\Models\RegistrationFee;
use App\Jobs\NotifyRecipientPaid;
use App\Jobs\NotifyUserConfirmed;
use App\Jobs\NotifyUserActivated;
use App\Jobs\NotifyAdminUserHasPaid;
use App\Repositories\PaymentsRepository;

class PaymentController extends Controller
{
	/**
	 * @var PaymentsRepository
	 */
	private $repository;

	function __construct(PaymentsRepository $repository)
	{
		//$this->middleware(['is-activated'])->except(['activate', 'doActivate']);
		$this->repository = $repository;
	}

	public function activate()
	{
		return redirect()->route('contribute');
		if (auth()->user()->isActive())
			return redirect()->route('home');

		return view('payments.activate');
    }

	public function doActivate(Request $request)
	{
		$this->validate($request, ['feeId' => 'required']);

		$registration = RegistrationFee::find($request->feeId);
		$this->authorize('pay', $registration);

		auth()->user()->registrationFee->setPaid();

		$this->dispatch(new NotifyAdminUserHasPaid(auth()->user()->registrationFee));

		return redirect()->back()
			->with('message', 'Your payment update has been recieved. Once the payment is confirmed, you will be notified to start making your contributions.')
			->with('type', 'success');
    }

	public function showConfirmRegistrationPage() {
		$registrations = RegistrationFee::latestFirst()->paginate(20);

		return view('admin.registrations', compact('registrations'));
	}

	public function confirmRegistration(Request $request)
	{
		$this->validate($request, ['feeId' => 'required']);

		$fee = RegistrationFee::find($request->feeId);
		$this->authorize('confirm', $fee);

		$fee->setConfirmed()->user->activate();

		$this->dispatch(new NotifyUserActivated($fee->user));

		return redirect()->back()
			->with('message', $fee->user->name . "'s payment confirmed and account activated.")
			->with('type', 'success');
    }

	public function denyRegistration( Request $request, RegistrationFee $fee ) {
		$this->authorize('confirm', $fee);

		$fee->deny();

		$this->dispatch(new NotifyUserDenied($fee->user));

		return redirect()->back()
		                 ->with('message', $fee->user->name . "'s payment denied.")
		                 ->with('type', 'warning');
    }

	public function contribute()
	{
		$data['transactions'] = $this->repository->fetchContributionsFor(auth()->user());

		return view('payments.make', $data);
    }
	
	/**
	 * @param Request $request
	 * @param Contribution $contribution
	 *
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function doContribute( Request $request, Contribution $contribution ) {
		$this->authorize('contribute', $contribution);
		
		if ($request->hasFile('upload')) {
			if(!$contribution->saveProof($request->file('upload'))) {
				return back()
					->with('message', 'Unable to store image. Pls use only JPEG or PNG images.')
					->with('type', 'danger');
			}
		}
		
		$contribution->setPaid();

		$this->dispatch(new NotifyRecipientPaid($contribution));

		return redirect()->back()
			->with('message', $contribution->recipient->name . " will be notified of your payment via email. You may place a call to speed up the confirmation.")
			->with('type', 'success');
    }


	public function confirmContribution( Contribution $contribution ) {
		$this->authorize('confirm', $contribution);

		$contribution->setConfirmed()->user->createCertsIfReady();

		$this->dispatch(new NotifyUserConfirmed($contribution));

		return redirect()->back()
             ->with('message', "You have successfully confirmed " . $contribution->user->name . "'s payment.")
             ->with('type', 'success');
    }

	public function contributions() {
		$contributions = auth()->user()->benefits()->with('user')->latestFirst()->paginate(20);

		return view('dashboard.payments', compact('contributions'));
    }

	public function voguePaymentNotification() {
		$trans_id = request()->get('transaction_id');

		$vogueResponse = Curl::get('https://voguepay.com/?type=json&v_transaction_id='.$trans_id)
			->asJsonResponse()->get();

		if (!isset($vogueResponse->ERROR)) {
			if ( $vogueResponse->status == "Approved" ) {
				$fee = RegistrationFee::find($vogueResponse->merchant_ref);

				if ($vogueResponse->total >= $fee->amount) {
					$fee->setConfirmed()->user->activate();

					$this->dispatch(new NotifyUserActivated($fee->user));
				}
			}
		}
    }
}
