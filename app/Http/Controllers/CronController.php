<?php
/**
 * Created by PhpStorm.
 * User: peterojo
 * Date: 6/19/17
 * Time: 11:37 PM
 */

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Models\Contribution;
use App\Models\RegistrationFee;
use App\Jobs\NotifyRecipientPaid;
use Illuminate\Support\Facades\Mail;
use App\Mail\UserAccountDeactivated;

class CronController extends Controller {
	public function sweepForNonPayers() {
		// to run every 12 hours
		// checks for unpaid reg fees
		$fees = RegistrationFee::unpaid()->get();
		$fees->each(function($fee) {
			if ($fee->created_at->addDays(3)->isPast()) {
				// notify the user of deactivation
				Mail::to($fee->user)->queue(new UserAccountDeactivated($fee->user));
				
				if ($fee->user->hasReferrer()) {
					// make certificate available again and unlink form user
					$fee->user->unlinkReferrerCertificate();
				}
				// soft delete the user
				$fee->user->delete();
				$fee->delete();
			}
		});
	}

	public function treatPendingContributions() {
        # find contributions paid more than 4 days ago and not confirmed
        $fourDaysAgo = Carbon::now()->subDays(4)->toDateTimeString();
        $pending4days = Contribution::awaitingConfirmation()->where('updated_at', '<', $fourDaysAgo)->get();

        # confirm them
        $pending4days->each(function ($contribution) {
            $contribution->setConfirmed()->user->createCertsIfReady();
        });

        # find contributions paid more than 2 days ago
        $twoDaysAgo = Carbon::now()->subDays(2)->toDateTimeString();
        $pending2days = Contribution::awaitingConfirmation()->where('updated_at', '<', $twoDaysAgo)->get();

        # dispatch emails to the recipients again
        $pending2days->each(function ($contribution) {
            $this->dispatch(new NotifyRecipientPaid($contribution));
        });
    }
}