<?php

namespace App\Http\Controllers;

use Auth;
use App\Http\Requests\BankAccountCreateRequest;
use App\Models\Account;
use Illuminate\Http\Request;

class BankAccountController extends Controller
{
	function __construct() {
		$this->middleware('auth');
	}

	public function create() {
		return view('account.create');
    }

	public function store( BankAccountCreateRequest $request ) {
		# grab the user id
		$user_id = Auth::id();

		Account::create([
			'user_id' => $user_id,
			'bank' => $request->bank_name,
			'number' => $request->account_number,
			'name' => $request->account_holder
		]);

		# redirect
		return redirect()->intended('/dashboard');
    }
}
