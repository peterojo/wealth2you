<?php

namespace App\Http\Controllers;

use App\Models\Certificate;
use App\Models\Contribution;
use App\Models\Level;
use App\Models\User;
use App\Repositories\PaymentsRepository;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     *
     */
    public function __construct()
    {
        $this->middleware(['has-certificates']);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function dashboard()
    {
    	$data['earned'] = auth()->user()->benefits()->confirmed()->sum('amount');
    	$data['paid'] = auth()->user()->contributions()->confirmed()->sum('amount');
		$data['highest_level'] = auth()->user()->highestLevel();
		$data['num_downlines'] = auth()->user()->assignedCertificates()->count();

        return view('dashboard.index', $data);
    }

	public function showDownlines( User $user ) {
		if (!$user->exists)
			$user = auth()->user();

		$certificates = $user->assignedCertificates()->with('assignee')->get();

		return view('dashboard.downlines', compact('certificates', 'user'));
    }

	public function autoAssign( Certificate $certificate, $option ) {
		$this->authorize('autoAssign', $certificate);

		$certificate->availability = (int) $option == 1 ? 1 : null;
		$certificate->save();

		$action = ['revoked', 'queued'];

		return redirect()->back()
			->with('message', 'Certificate ' . $certificate->code . ' has been ' . $action[$option] . ' for automatic assignment.')
			->with('type', 'success');
    }
}
