<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;

class VerifyCsrfToken extends BaseVerifier
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        'vogue-notification-webhook'
    ];

    public function handle( $request, Closure $next )
    {
	    if (
		    $this->isReading($request) ||
		    $this->runningUnitTests() ||
		    $this->inExceptArray($request) ||
		    $this->tokensMatch($request)
	    ) {
		    return $this->addCookieToResponse($request, $next($request));
	    }

	    return redirect()->back()->with('status', 'Session Token Mismatch')->with('type', 'danger');
    }
}
