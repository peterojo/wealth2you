<?php

namespace App\Http\Middleware;

use Closure;

class AdminAccess
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
    	if( !auth()->user()->isAdmin() ) {
    		return redirect()->route('home')->with('message', 'You are not an admin')->with('type', 'danger');
	    }

        return $next($request);
    }
}
