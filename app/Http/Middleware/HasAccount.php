<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class HasAccount
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::check() && Auth::user()->account === null) {
            return redirect()->route('account')->with('status', 'Please register your bank account before proceeding.')
			    ->with('type', 'info');
	    }

        return $next($request);
    }
}
