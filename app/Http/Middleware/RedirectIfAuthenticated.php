<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->check()) {
            return redirect('/dashboard')
	            ->with('message', "<strong>PLEASE NOTE</strong><br>You are already logged in as <strong>" . auth()->user()->name . "</strong>.<br>If you wish to register a new account or login with a different account...<br>then <a href='".route('logout')."' onclick=\"logOut(event);\">click here to log out.</a>")
	            ->with('type', 'danger');
        }

        return $next($request);
    }
}
