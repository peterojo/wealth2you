<?php

namespace App\Http\Middleware;

use Closure;

class IsActivated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
		if (!auth()->user()->isActive()) {
			return redirect()->route('activate')
				->with('status', 'Kindly make one-time payment to Wealth to You in order to start contributing')
				->with('type', 'info');
		}

        return $next($request);
    }
}
