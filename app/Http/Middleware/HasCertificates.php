<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class HasCertificates
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
    	if (!Auth::user()->hasCertificates())
    		return redirect()->route('contribute')
			    ->with('status', 'Kindly make one-time payment to Wealth to You and then contribute to the accounts below.')
			    ->with('type', 'warning');

        return $next($request);
    }
}
