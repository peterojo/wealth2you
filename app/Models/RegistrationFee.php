<?php

namespace App\Models;

use App\Models\Traits\Payable;
use App\Models\Traits\Sortable;
use Illuminate\Database\Eloquent\Model;

class RegistrationFee extends Model
{
	use Payable, Sortable;
    protected $fillable = ['currency', 'amount'];

	public function user() {
		return $this->belongsTo(User::class);
    }
}
