<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Testimonial extends Model
{
    protected $fillable = ['title', 'body'];

    protected $with = ['user'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function scopeEnabled($query)
    {
        return $query->where('enabled', 1);
    }

    public function enable()
    {
        $this->enabled = 1;
        $this->save();
    }

    public function disable()
    {
        $this->enabled = 0;
        $this->save();
    }
}
