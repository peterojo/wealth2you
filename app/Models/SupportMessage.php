<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SupportMessage extends Model
{
    protected $fillable = ['user_id', 'message', 'read'];

	public function ticket() {
		return $this->belongsTo(SupportTicket::class, 'ticket_id');
    }

	public function user() {
		return $this->belongsTo(User::class);
    }

	public function markAsRead() {
		$this->read = 1;
		$this->save();
    }
}
