<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SupportTicket extends Model
{
    protected $fillable = ['user_id', 'ticket_code', 'subject'];

	public function messages() {
		return $this->hasMany(SupportMessage::class, 'ticket_id');
    }

	public function user() {
		return $this->belongsTo(User::class);
    }

	public function transaction() {
		return $this->belongsTo(Contribution::class, 'trans_id', 'trans_id');
    }

	public function getRouteKeyName() {
		return "ticket_code";
    }
}
