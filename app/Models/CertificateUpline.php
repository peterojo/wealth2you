<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CertificateUpline extends Model
{
    protected $table = "certificate_uplines";
    
    protected $fillable = ['ref_cert_level_id', 'ref_cert_id'];
	
	public function certificate () {
		return $this->belongsTo(Certificate::class, 'cert_id', 'id');
    }
	
	public function referrer () {
		return $this->belongsTo(Certificate::class, 'ref_cert_id', 'id');
    }
	
	public function level () {
		return $this->belongsTo(Level::class, 'ref_cert_level_id', 'id');
    }
	
	public function promote() {
		if($this->ref_cert_level_id < 8) {
			$this->ref_cert_level_id++;
		} else {
			// cycle user out of system
			$this->cycled_out = 1;
		}
		$this->save();
		
		if ($this->referrer->owner->isReadyToCycleOut()) {
			$this->referrer->owner->delete();
		}
	}
	
	public function isOut () {
		return (bool) $this->cycled_out;
	}
}
