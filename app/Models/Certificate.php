<?php

namespace App\Models;

use App\Services\SMS;
use App\Mail\CertificateAssigned;
use Illuminate\Support\Facades\Mail;
use Illuminate\Database\Eloquent\Model;
use App\Repositories\PaymentsRepository;

class Certificate extends Model
{
    protected $fillable = ['code', 'level_id'];

	public function getRouteKeyName() {
		return "code";
    }

	public function owner() {
		return $this->belongsTo(User::class, 'owner_id', 'id');
	}

	public function assignee() {
		return $this->hasOne(User::class, 'ref_cert_id', 'id');
	}

	public function level() {
		return $this->belongsTo(Level::class);
	}
	
	public function uplines () {
		return $this->hasMany(CertificateUpline::class, 'cert_id', 'id');
	}
	
	public function downlines () {
		return $this->hasMany(CertificateUpline::class, 'ref_cert_id', 'id');
	}

	public function scopeUpForGrabs( $query ) {
		return $query->where('availability', 1);
	}

	public function scopeWithSamePackage( $query, $package ) {
		return $query->whereHas('owner', function ($query) use ($package) {
			$query->whereHas('package', function ($query) use ($package) {
				$query->where('currency', '=', $package['currency'])
				      ->where('amount', '=', $package['amount']);
			});
		});
	}

	public function paymentsAccountListing() {
		return (new PaymentsRepository())->getCertificateList($this);
	}
	
	public function uplineAccountListing () {
		$uplines =  $this->uplines()->with('referrer.owner.account')->get();;
		$accounts = [];
		$uplines->each(function($upline) use (&$accounts) {
			$accounts[] = $upline->referrer->owner->account;
		});
		
		return $accounts;
	}

	public function isTaken() {
		return (bool) $this->assignee;
	}

	public function isUpForGrabs() {
		return (bool) $this->availability;
	}

	public function assignTo( $userId ) {
		$this->assignee_id = $userId;
		$this->availability = 0;
		$this->taken = 1;
		$this->save();
		
		$user = User::find($userId);
		$user->ref_cert_id = $this->id;
		$user->save();

        (new SMS)->send($this->owner->phone, "Your certificate ".$this->code." has been assigned.");
		Mail::to($this->owner)->queue(new CertificateAssigned($this));
	}

	public function promote() {
		/*if($this->level_id < 8) {
			$this->level_id++;
			$this->save();
		} else {
			// todo cycle user out of system
		}*/
		
		$this->promoteUplines();
/*
		if ($this->owner->hasReferrer()) {
			$this->owner->referrerCertificate->promote();
		}*/
	}
	
	public function promoteUplines () {
		$this->uplines->each(function($upline) {
			$upline->promote();
		});
	}
	
	public function createUplines () {
		$recipients = (new PaymentsRepository)->certificateUplinesList($this);
		$i = 1;
		foreach ( $recipients as $recipient ) {
			if ($i <= 8) {
				$this->uplines()->create([
					'ref_cert_id' => $recipient->id,
					'ref_cert_level_id' => $i
				]);
			}
			$i++;
		}
	}
	
	public function makeAvailable () {
		$this->availability = 1;
		$this->taken = 0;
		$this->assignee_id = NULL;
		
		return $this->save();
	}
	
	protected static function boot () {
		parent::boot();
		
		static::created(function($certificate) {
			$certificate->createUplines();
		});

		static::deleting(function($certificate) {
		    $certificate->assignee->ref_cert_id = null;
		    $certificate->assignee->save();
		    $certificate->downlines()->delete();
		    $certificate->uplines()->delete();
        });
	}
}
