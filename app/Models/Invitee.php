<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Invitee extends Model
{
    protected $fillable = ['name', 'email', 'phone', 'invite_code'];

	public function inviter() {
		return $this->belongsTo(User::class, 'inviter_id');
    }
    
    public function getRouteKeyName () {
	    return "invite_code";
    }
	
	public function markAsJoined () {
		$this->joined = 1;
		$this->save();
    }
}
