<?php
/**
 * Created by PhpStorm.
 * User: peterojo
 * Date: 5/28/17
 * Time: 12:05 AM
 */

namespace App\Models\Traits;


trait Sortable {
	public function scopeLatestFirst( $query ) {
		return $query->orderBy('created_at', 'DESC');
	}
}