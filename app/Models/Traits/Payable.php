<?php

namespace App\Models\Traits;

trait Payable {
	public function isPaid() {
		return (bool) $this->paid;
	}

	public function isConfirmed() {
		return (bool) $this->confirmed;
	}

	public function isAwaitingConfirmation() {
		return $this->isPaid() && !$this->isConfirmed();
	}

	public function setPaid() {
		$this->paid = 1;
		$this->save();
	}

	public function setConfirmed() {
		$this->confirmed = $this->paid = 1;
		$this->save();

		return $this;
	}

	public function deny() {
		$this->confirmed = $this->paid = 0;
		$this->save();
	}

	public function scopePaid($query) {
		return $query->where('paid', 1);
	}

	public function scopeUnpaid($query) {
		return $query->where('paid', 0);
	}

	public function scopeConfirmed($query) {
		return $query->where('confirmed', 1);
	}

    public function scopeAwaitingConfirmation($query)
    {
        return $query->where('paid', 1)->where('confirmed', 0);
	}
}