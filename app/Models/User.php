<?php

namespace App\Models;

use App\Events\UserDeleted;
use Carbon\Carbon;
use App\Libraries\Misc;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable, SoftDeletes;

    protected $table = "users";

	/**
	 * The attributes that should be mutated to dates.
	 *
	 * @var array
	 */
	protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'phone', 'ref_cert_id', 'code'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $events = [
        'deleting' => UserDeleted::class
    ];

	public function getRouteKeyName() {
		return "code";
    }

    # Relationships

	public function certificates() {
		return $this->hasMany(Certificate::class, 'owner_id', 'id');
	}

	public function referrerCertificate() {
		return $this->belongsTo(Certificate::class, 'ref_cert_id', 'id');
	}

	public function registrationFee() {
		return $this->hasOne(RegistrationFee::class);
	}

	public function invitees() {
		return $this->hasMany(Invitee::class, 'inviter_id');
    }

	public function downlines() {
		return $this->hasManyThrough(User::class, Certificate::class, 'owner_id', 'ref_cert_id', 'id');
    }

	public function package() {
		return $this->hasOne(Package::class);
    }

	public function account() {
		return $this->hasOne(Account::class);
    }

	public function contributions() {
		return $this->hasMany(Contribution::class);
    }

	public function benefits() {
		return $this->hasMany(Contribution::class, 'recipient_id', 'id');
    }

	public function tickets() {
		return $this->hasMany(SupportTicket::class);
    }

    public function testimonials()
    {
        return $this->hasMany(Testimonial::class);
    }

    # helpers
	public function hasCertificates() {
		return $this->certificates->count() > 0;
	}

	public function hasReferrer() {
		return $this->referrerCertificate !== null;
	}

	public function isActive() {
		return (bool) $this->status;
	}

	public function completedContributions() {
		$complete = true;

		if ($this->contributions->count() == 0)
			return false;

		$this->contributions->each(function($contribution) use (&$complete) {
			if (!$contribution->isConfirmed()) {
				$complete = false;
				return false;
			}
		});

		return $complete;
	}
	
	public function isReadyToCycleOut () {
		$done = true;
		
		$this->certificates->each(function ($certificate) use (&$done) {
			$certificate->downlines->each(function ($upline) use (&$done) {
				if (!$upline->isOut()) {
					$done = false;
					return false;
				}
			});
			return $done;
		});
		
		return $done;
	}

    public function isAdmin() {
        return $this->role == "admin";
    }

	# Setter helpers
	public function activate () {
		$this->status = 1;
		$this->save();
	}
	
	public function deactivate () {
		$this->status = 0;
		$this->save();
		
		return $this;
	}
	
	public function enable () {
		$this->enabled = 1;
		$this->save();
	}

	public function createCertsIfReady() {
		if ($this->completedContributions()) {
			if (!$this->hasCertificates()) {
				$this->certificates()->createMany([
					[
						'code' => Misc::generateCertificateCode()
					],
					[
						'code' => Misc::generateCertificateCode()
					],
					[
						'code' => Misc::generateCertificateCode()
					]
				]);
			}
		}
	}

    public function unlinkReferrerCertificate () {
        if ($this->hasReferrer()) {
            $this->referrerCertificate->makeAvailable();
            $this->ref_cert_id = NULL;

            return $this->save();
        }
    }

    public function delete () {
        $this->email = null;
        $this->save();

        return parent::delete();
    }

	# Getter helpers
	public function assignedCertificates() {
		return $this->certificates()->has('assignee');
	}

	public function latestTicket() {
		return $this->tickets()->orderBy('created_at', 'DESC')->first();
	}

	public function availableCertificate() {
		$availableCertificate = $this->certificates()->upForGrabs()->first();
		if ($availableCertificate) {
			return $availableCertificate;
		}

		// find next available cerfiticate
		return $this->digDownForNextAvailableCertificate();
	}

	/**
	 * Drill down recursively through downlines
	 * until an available certificate is found
	 * @return null|Certificate
	 */
	public function digDownForNextAvailableCertificate() {
		// check if user has downlines at all
		if (!$this->downlines()->first()) {
			return null;
		}

		// get the first downline with available certificate
		$downlineWithAvailableCert = $this->downlinesWithAvailableCertificates()->first();
		if ($downlineWithAvailableCert) {
			// return the certificate
			return $downlineWithAvailableCert->certificates()->upForGrabs()->first();
		} else {
			// if no downline with available certificate
			// then loop through all downlines and repeat the whole process for each one
			$nextCertificate = null;
			foreach ($this->downlines as $downline) {
				if (!$downline->digDownForNextAvailableCertificate()) {
					continue;
				} else {
					$nextCertificate = $downline->digDownForNextAvailableCertificate();
				}
			}
			return $nextCertificate;
		}
	}

	public function scopeDownlinesWithAvailableCertificates( $query ) {
		return $query->whereHas('certificates', function($query) {
			$query->upForGrabs();
		});
	}
	
	public function highestLevel () {
		$certDownlines = [0];
		
		$this->certificates->each(function($cert) use (&$certDownlines) {
			if ($cert->downlines->count() > 0) {
				$certDownlines[] = $cert->downlines->max('ref_cert_level_id');
			}
		});
		
		return max($certDownlines) == 0 ? "Crystal" : Level::find(max($certDownlines))->name;
	}
}