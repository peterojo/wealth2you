<?php

namespace App\Services;

use Ixudra\Curl\Facades\Curl;

class SMS {
    const GATEWAY_ENDPOINT = "http://bzbulksms.com/components/com_spc/smsapi.php";

    const SMS_USERNAME = "w2u";
    const SMS_PASSWORD = "divine2016";

    public function send($recipients, $message)
    {
        return Curl::to($this->buildQuery($recipients, $message))->get();
    }

    private function buildQuery($recipients, $message)
    {
        if (is_array($recipients)) {
            $recipients = join(",", $recipients);
        }

        return self::GATEWAY_ENDPOINT."?username=".self::SMS_USERNAME."&password=".self::SMS_PASSWORD
            ."&sender=Wealth2You&recipient=".$recipients."&message=".urlencode($message);
    }

    public function paidMessage()
    {
        return "You have been paid.";
    }
}