<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Certificate;
use Illuminate\Auth\Access\HandlesAuthorization;

class CertificatePolicy
{
    use HandlesAuthorization;

	public function autoAssign( User $user, Certificate $certificate ) {
		return $user->id === $certificate->owner->id;
    }
}
