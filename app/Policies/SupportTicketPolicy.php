<?php

namespace App\Policies;

use App\Models\User;
use App\Models\SupportTicket;
use Illuminate\Auth\Access\HandlesAuthorization;

class SupportTicketPolicy
{
    use HandlesAuthorization;

	public function addMessage( User $user, SupportTicket $ticket ) {
		return ($user->id === $ticket->user->id) || $user->isAdmin();
	}
}
