<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Contribution;
use Illuminate\Auth\Access\HandlesAuthorization;

class ContributionPolicy
{
    use HandlesAuthorization;

	public function contribute( User $user, Contribution $contribution ) {
		return $user->id === $contribution->user->id;
    }

	public function confirm( User $user, Contribution $contribution ) {
		return $user->id === $contribution->recipient->id;
    }

	public function createTicket( User $user, Contribution $contribution ) {
		return $user->id === $contribution->user->id || $user->id === $contribution->recipient->id;
    }
}
