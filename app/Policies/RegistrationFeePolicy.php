<?php

namespace App\Policies;

use App\Models\User;
use App\Models\RegistrationFee;
use Illuminate\Auth\Access\HandlesAuthorization;

class RegistrationFeePolicy
{
    use HandlesAuthorization;

	public function pay( User $user, RegistrationFee $registration )
	{
		return $user->id === $registration->user->id;
    }

	public function confirm( User $user, RegistrationFee $registration )
	{
		// @todo: check that the signed in user is some kind of w2u admin
		return true;
    }
}
