<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use App\Models\RegistrationFee;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class AdminUserPaid extends Mailable
{
    use Queueable, SerializesModels;

	protected $fee;

	/**
	 * Create a new message instance.
	 *
	 * @param RegistrationFee $fee
	 */
    public function __construct(RegistrationFee $fee)
    {
        $this->fee = $fee;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject("Registration Fee Payment update: " . $this->fee->user->name)
	        ->view('emails.reg-paid')
	        ->with([
	        	'payer' => $this->fee->user->name,
		        'amount' => $this->fee->amount,
		        'currency' => $this->fee->currency,
		        'link' => route('admin.registrations')
	        ]);
    }
}
