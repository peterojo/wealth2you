<?php

namespace App\Mail;

use App\Models\Contribution;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class UserConfirmed extends Mailable
{
    use Queueable, SerializesModels;
	/**
	 * @var Contribution
	 */
	private $contribution;

	/**
	 * Create a new message instance.
	 *
	 * @param Contribution $contribution
	 */
    public function __construct(Contribution $contribution)
    {
	    $this->contribution = $contribution;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject("Your contribution was confirmed")
	        ->view('emails.confirmed')
	        ->with([
	        	'user' => $this->contribution->user->name,
		        'recipient' => $this->contribution->recipient->name,
		        'link' => route('contribute')
	        ]);
    }
}
