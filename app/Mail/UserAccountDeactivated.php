<?php

namespace App\Mail;

use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class UserAccountDeactivated extends Mailable
{
    use Queueable, SerializesModels;
	/**
	 * @var User
	 */
	protected $user;

	/**
	 * Create a new message instance.
	 *
	 * @param User $user
	 */
    public function __construct(User $user)
    {
	    $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Your account has been deactivated.')
	        ->view('emails.deactivated')
	        ->with([
	        	'user' => $this->user->name,
		        'link' => route('reactivate', $this->user->code)
	        ]);
    }
}
