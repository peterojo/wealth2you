<?php

namespace App\Mail;

use App\Models\Certificate;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class CertificateAssigned extends Mailable
{
    use Queueable, SerializesModels;
	/**
	 * @var Certificate
	 */
	private $certificate;
	
	/**
	 * Create a new message instance.
	 *
	 * @param Certificate $certificate
	 */
    public function __construct(Certificate $certificate)
    {
        //
	    $this->certificate = $certificate;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject("Your certificate (".$this->certificate->code.") has been assigned.")
                    ->view('emails.assigned')
	                ->with([
						'name' => $this->certificate->owner->name,
						'certCode' => $this->certificate->code,
						'recipient' => $this->certificate->assignee->name,
		                'link' => route('login')
	                ]);
    }
}
