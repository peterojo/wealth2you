<?php

namespace App\Mail;

use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class UserDenied extends Mailable
{
    use Queueable, SerializesModels;
	/**
	 * @var User
	 */
	protected $user;

	/**
	 * Create a new message instance.
	 *
	 * @param User $user
	 */
    public function __construct(User $user)
    {
	    $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
	    return $this->subject("Your payment was denied.")
	                ->view('emails.denied')
	                ->with([
		                'user' => $this->user->name,
		                'link' => route('activate')
	                ]);
    }
}
