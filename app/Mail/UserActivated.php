<?php

namespace App\Mail;

use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class UserActivated extends Mailable
{
    use Queueable, SerializesModels;
	/**
	 * @var User
	 */
	private $user;

	/**
	 * Create a new message instance.
	 *
	 * @param User $user
	 */
    public function __construct(User $user)
    {
	    $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject("Your account has been activated.")
	        ->view('emails.activated')
	        ->with([
	        	'user' => $this->user->name,
		        'link' => route('contribute')
	        ]);
    }
}
