<?php

namespace App\Jobs;

use App\Mail\UserConfirmed;
use App\Models\Contribution;
use App\Services\SMS;
use Illuminate\Bus\Queueable;
use Illuminate\Support\Facades\Mail;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class NotifyUserConfirmed implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
	/**
	 * @var Contribution
	 */
	private $contribution;

	/**
	 * Create a new job instance.
	 *
	 * @param Contribution $contribution
	 */
    public function __construct(Contribution $contribution)
    {
	    $this->contribution = $contribution;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        (new SMS)->send($this->contribution->user->phone, "Congratulations! Your payment has been confirmed.");
        Mail::to($this->contribution->user)->send(new UserConfirmed($this->contribution));
    }
}
