<?php

namespace App\Jobs;

use App\Mail\AdminUserPaid;
use Illuminate\Bus\Queueable;
use App\Models\RegistrationFee;
use Illuminate\Support\Facades\Mail;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class NotifyAdminUserHasPaid implements ShouldQueue
{
	use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
	/**
	 * @var RegistrationFee
	 */
	private $fee;

	/**
	 * Create a new job instance.
	 *
	 * @param RegistrationFee $fee
	 */
    public function __construct(RegistrationFee $fee)
    {
	    $this->fee = $fee;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Mail::to(config('w2u.admin.email'))->send(new AdminUserPaid($this->fee));
    }
}
