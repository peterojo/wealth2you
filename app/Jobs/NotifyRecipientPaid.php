<?php

namespace App\Jobs;

use App\Services\SMS;
use App\Mail\RecipientPaid;
use App\Models\Contribution;
use Illuminate\Bus\Queueable;
use Illuminate\Support\Facades\Mail;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class NotifyRecipientPaid implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
	private $contribution;

	/**
	 * Create a new job instance.
	 *
	 * @param Contribution $contribution
	 */
    public function __construct(Contribution $contribution)
    {
        $this->contribution = $contribution;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        (new SMS)->send($this->contribution->recipient->phone, "Congratulations! You have been paid");
        Mail::to($this->contribution->recipient)->send(new RecipientPaid($this->contribution));
    }
}
