<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
	    $this->registerPolicies();

	    //
    }

    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Models\Certificate' => 'App\Policies\CertificatePolicy',
        'App\Models\Testimonial' => 'App\Policies\TestimonialPolicy',
        'App\Models\Contribution' => 'App\Policies\ContributionPolicy',
        'App\Models\SupportTicket' => 'App\Policies\SupportTicketPolicy',
        'App\Models\RegistrationFee' => 'App\Policies\RegistrationFeePolicy',
    ];
}
