<?php
/**
 * Created by PhpStorm.
 * User: peterojo
 * Date: 4/23/17
 * Time: 11:24 PM
 */

namespace App\Libraries;

use Illuminate\Support\Facades\DB;

/**
 * Class Misc
 * @package App\Libraries
 */
class Misc {
	/**
	 * @return string
	 */
	public static function generateUniqueRefID()
	{
		$code = strtolower(str_random(6));

		$count = DB::table('users')->where('code', '=', $code)->count();
		if($count != 0){
			return self::generateUniqueRefID();
		}

		return $code;
	}

	public static function generateTransactionID() {
		$trans_id = str_random(12);

		$count = DB::table('contributions')->where('trans_id', '=', $trans_id)->count();
		if($count != 0){
			return self::generateTransactionID();
		}

		return $trans_id;
	}

	public static function generateTicketCode() {
		$ticket_code = str_random(16);

		$count = DB::table('support_tickets')->where('ticket_code', '=', $ticket_code)->count();
		if($count != 0){
			return self::generateTicketCode();
		}

		return $ticket_code;
	}

	public static function generateCertificateCode() {
		$code = str_random(8);

		$count = DB::table('certificates')->where('code', '=', $code)->count();
		if($count != 0){
			return self::generateCertificateCode();
		}

		return $code;
	}
}