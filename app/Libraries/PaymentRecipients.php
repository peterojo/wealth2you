<?php
/**
 * Created by PhpStorm.
 * User: peterojo
 * Date: 4/27/17
 * Time: 9:22 PM
 */

namespace App\Libraries;

use App\Models\Contribution;
use App\Models\NeedsDownline;
use App\Models\User;

class PaymentRecipients {

	public static function getContributions( User $user ) {
		if (!empty($user->contributions->all())) {
			return $user->contributions;
		}

		// else get recipients and create contributions
		$recipients = self::getRecipients($user);

		if ($recipients==null)
			return null;

		foreach ( $recipients as $recipient ) {
			Contribution::create([
				'trans_id' => Misc::generateTransactionID(),
				'user_id' => $user->id,
				'recipient_id' => $recipient->id,
				'currency' => $user->package->currency,
				'amount' => $user->package->amount
			]);
		}

		return Contribution::where('user_id', $user->id)->get();
	}



}