<?php
/**
 * Created by PhpStorm.
 * User: peterojo
 * Date: 5/13/17
 * Time: 2:03 PM
 */

namespace App\Repositories;

use App\Models\User;
use App\Libraries\Misc;
use App\Models\Certificate;
use App\Models\Contribution;

class PaymentsRepository {
	public function fetchContributionsFor( User $user ) {
		if (!empty($user->contributions->all())) {
			return $user->contributions->reverse();
		}

		// else get recipients and create contributions
		$recipients = $this->getRecipients($user);

		foreach ( $recipients as $recipient ) {
			Contribution::create([
				'trans_id' => Misc::generateTransactionID(),
				'user_id' => $user->id,
				'recipient_id' => $recipient->id,
				'currency' => $user->package->currency,
				'amount' => $user->package->amount
			]);
		}

		return Contribution::where('user_id', $user->id)->get()->reverse();
	}

	/**
	 * @param User $user
	 * @return array
	 */
	public function getRecipients(User $user)
	{
		if ( $user->hasReferrer() ) {
			return $this->ancestryList($user);
		}
		// if user has no referrer,
		// assign one from available certificates
		$nextCert = Certificate::upForGrabs()
			->withSamePackage( [
				'currency' => $user->package->currency,
				'amount'   => $user->package->amount
			] )->first();

		if ( $nextCert ) {
			$user->ref_cert_id = $nextCert->id;
			$user->save();

			$nextCert->assignTo($user->id); // notify cert owner
			$nextCert->promote();
		} else {
			return []; // no available waiting upline for this user @todo recycle old preset users
		}

		return $this->ancestryList($user);
	}

	/**
	 * @param User $user
	 *
	 * @return array
	 */
	public function ancestryList(User $user)
	{
		$uplines = [];
		while ($user->hasReferrer() && count($uplines)<8) {
			$uplines[] = $user = $user->referrerCertificate->owner;
		}

		return $uplines;
	}
	
	public function certificateUplinesList ( Certificate $certificate ) {
		$uplines = [$certificate];
		while ($certificate->owner->hasReferrer()) {
			$uplines[] = $certificate = $certificate->owner->referrerCertificate;
		}
		
		return $uplines;
	}
	
	public function getCertificateList( Certificate $certificate ) {
		$owner = $certificate->owner;
		$list = [];
		for ($i=0; $i<8; $i++) {
			if ($owner->account)
				$list[$i] = $owner->account;

			if (!$owner->referrerCertificate)
				break;

			$owner = $owner->referrerCertificate()->with('owner')->first()->owner->account;
		}
		
		return collect($list)->reverse();
	}
}