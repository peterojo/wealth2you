<?php

namespace App\Listeners;

use App\Events\UserDeleted;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class UntangleDeletedUserFromSystem
{
    /**
     * Handle the event.
     *
     * @param  UserDeleted  $event
     * @return void
     */
    public function handle(UserDeleted $event)
    {
        $user = $event->user;
        $user->certificates()->delete();
        $user->contributions()->delete();
        $user->package()->delete();
        $user->account()->delete();
        $user->invitees()->delete();
        $user->testimonials()->delete();
    }
}
