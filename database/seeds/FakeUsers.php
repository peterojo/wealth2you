<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use App\Models\User;

class FakeUsers extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();

        for ($i=8;$i>0;$i--) {
        	User::create([
        		'name' => $faker->name,
		        'email' => $faker->email,
		        'password' => bcrypt('secret'),
		        'phone' => $faker->phoneNumber,
		        'level_id' => $i,
		        'ref_id' => \App\Libraries\Misc::generateUniqueRefID()
	        ]);

        }
    }
}
