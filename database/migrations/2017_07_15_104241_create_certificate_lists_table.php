<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCertificateListsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('certificate_uplines', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('cert_id');
            $table->unsignedInteger('ref_cert_id');
            $table->unsignedInteger('ref_cert_level_id');
            $table->boolean('cycled_out')->default(false);
            $table->timestamps();
	
	        $table->unique(['cert_id', 'ref_cert_id']);
	        $table->foreign('cert_id')->references('id')->on('certificates')->onDelete('cascade');
	        $table->foreign('ref_cert_id')->references('id')->on('certificates')->onDelete('cascade');
	        $table->foreign('ref_cert_level_id')->references('id')->on('levels')->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('certificate_uplines');
    }
}
