<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCertificatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('certificates', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code', 8);
            $table->integer('owner_id')->unsigned();
            $table->integer('level_id')->unsigned()->default(1);
            $table->integer('assignee_id')->unsigned()->nullable();
            $table->boolean('availability')->default(true);
            $table->boolean('taken')->default(false);
            $table->timestamps();

            $table->index('code');
            $table->foreign('level_id')->references('id')->on('levels');
            $table->foreign('owner_id')->references('id')->on('users')->onDelete('restrict');
            $table->foreign('assignee_id')->references('id')->on('users')->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('certificates');
    }
}
