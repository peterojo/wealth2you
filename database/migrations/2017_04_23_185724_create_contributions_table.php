<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContributionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contributions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('trans_id', 12)->index();
            $table->integer('user_id')->unsigned();
            $table->integer('recipient_id')->unsigned();
            $table->enum('currency', ['USD','NGN']);
            $table->integer('amount');
            $table->string('proof');
            $table->boolean('paid')->default(0);
            $table->boolean('confirmed')->default(0);
            $table->boolean('cancelled')->default(0);
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('recipient_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contributions');
    }
}
