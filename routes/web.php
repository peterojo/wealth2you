<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', ['as' => 'welcome', 'uses' => 'WelcomeController@index']);
Route::get('/about', ['as' => 'about', 'uses' => 'WelcomeController@about']);

Auth::routes();

Route::get('/check-email', 'Auth\RegisterController@checkEmail');
Route::get('/enable/{user}', ['as' => 'enable', 'uses' => 'Auth\LoginController@enable']);

Route::get('/account', ['as' => 'account', 'uses' => 'BankAccountController@create']);
Route::post('/account', ['as' => 'store.account', 'uses' => 'BankAccountController@store']);

Route::group(['middleware'=> ['auth', 'has-account']], function () {
	Route::get('/dashboard', ['as' => 'home', 'uses' => 'HomeController@dashboard']);

	Route::get('/pay-registration', ['as' => 'activate', 'uses' => 'PaymentController@activate']);
	Route::post('/pay-registration', ['as' => 'activate.post', 'uses' => 'PaymentController@doActivate']);

	Route::group(['middleware' => 'is-site-admin', 'prefix' => 'admin'], function() {
		Route::get('/confirm-registration', ['as' => 'admin.registrations', 'uses' => 'PaymentController@showConfirmRegistrationPage']);
		Route::post('/confirm-registration', ['as' => 'admin.confirm-registration', 'uses' => 'PaymentController@confirmRegistration']);
		Route::post('/deny-registration/{fee}', ['as' => 'admin.deny-registration', 'uses' => 'PaymentController@denyRegistration']);

		Route::get('/testimonials', ['as' => 'admin.testimonials', 'uses' => 'TestimonialController@index']);
		Route::post('/testimonials/enable/{testimonial}', ['as' => 'admin.enable-testimonial', 'uses' => 'TestimonialController@enable']);
		Route::post('/testimonials/disable/{testimonial}', ['as' => 'admin.disable-testimonial', 'uses' => 'TestimonialController@disable']);
	});

	Route::get('/make-contributions', ['as' => 'contribute', 'uses' => 'PaymentController@contribute']);
	Route::post('/contribute/{contribution}', ['as' => 'contribute.post', 'uses' => 'PaymentController@doContribute']);
	Route::post('/confirm/{contribution}', ['as' => 'contribute.confirm', 'uses' => 'PaymentController@confirmContribution']);

	Route::get('/support/{ticket?}', ['as' => 'support', 'uses' => 'SupportTicketController@show']);
	Route::post('/support-ticket', ['as' => 'ticket.store', 'uses' => 'SupportTicketController@store']);
	Route::post('/support-ticket/{ticket}/add-message', ['as' => 'ticket.store-message', 'uses' => 'SupportTicketController@storeTicketMessage']);

	Route::get('/invite', ['as' => 'invite', 'uses' => 'InviteController@index']);
	Route::post('/invite', ['as' => 'invite.store', 'uses' => 'InviteController@store']);

	Route::get('/downlines/{user?}', ['as' => 'downlines', 'uses' => 'HomeController@showDownlines']);
	Route::get('/contributions', ['as' => 'contributions', 'uses' => 'PaymentController@contributions']);

	Route::post('/certificate/{certificate}/auto-assign/{option}', ['as' => 'auto-assign', 'uses' => 'HomeController@autoAssign']);

	Route::get('/testimonials/create', ['as' => 'testimonial.create', 'uses' => 'TestimonialController@create']);
	Route::post('/testimonial', ['as' => 'testimonial.new', 'uses' => 'TestimonialController@store']);
});

Route::get('/reactivate-me/{user}', ['as' => 'reactivate', 'uses' => 'Auth\RegisterController@reactivateUserForm']);
Route::post('/vogue-notification-webhook', ['as' => 'vogue-webhook', 'uses' => 'PaymentController@voguePaymentNotification']);

Route::get('/cron/sweep-for-unpaid-fees', 'CronController@sweepForNonPayers');
Route::get('/cron/handle-pending-contributions', 'CronController@treatPendingContributions');

Route::get('/invite/{invitee}', ['as' => 'invite.show', 'uses' => 'InviteController@show']);

Route::get('/send-sms', 'SMSController@send');

Route::get('sandbox/{user}', function(\App\Models\User $user) {
	
	/*$exitCode = \Illuminate\Support\Facades\Artisan::call('storage:link');
	
	dd($exitCode);*/
	/*$certificates = \App\Models\Certificate::all();
	
	foreach ( $certificates as $certificate ) {
		$recipients = (new App\Repositories\PaymentsRepository)->certificateUplinesList($certificate);
		
		foreach ( $recipients as $recipient ) {
			$certificate->uplines()->create([
				'ref_cert_id' => $recipient->id,
				'ref_cert_level_id' => $recipient->level_id
			]);
		}
	}
	
	return $certificates;*/
	/*$user->certificates()->create([
		'code' => \App\Libraries\Misc::generateCertificateCode()
	]);

	return $user;*/
});

Route::get('ralph-logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');
