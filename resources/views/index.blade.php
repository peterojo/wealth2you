@extends('master')

@section('content')
    <!--Slider Main-->
    <section class="rev_slider_wrapper">
        <div id="rev_slider" class="rev_slider"  data-version="5.0">
            <ul>
                <!-- SLIDE  -->
                <li data-transition="fade">
                    <!-- MAIN IMAGE -->
                    <img src="{{ asset('images/home4-banner1.jpg') }}" alt="" data-bgposition="center center" data-bgfit="cover">
                    <!-- LAYER NR. 1 -->
                    <h1 class="tp-caption  tp-resizeme uppercase"
                        data-x="['left','left','left','center']" data-hoffset="['0','15','15','15']"
                        data-y="['260','160','160','90']" data-voffset="['0','0','0','0']"
                        data-responsive_offset="on"
                        data-transform_idle="o:1;"
                        data-transform_in="y:-50px;opacity:0;s:1500;e:Power3.easeOut;"
                        data-transform_out="s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;"
                        data-start="1000">Welcome To <strong class="blue_t">Wealth To You</strong>
                    </h1>
                    <div class="tp-caption  tp-resizeme"
                         data-x="['left','left','left','center']" data-hoffset="['0','15','15','15']"
                         data-y="['330','230','230','170']" data-voffset="['0','0','0','0']"
                         data-responsive_offset="on"
                         data-transform_idle="o:1;"
                         data-transform_in="y:-50px;opacity:0;s:1500;e:Power3.easeOut;"
                         data-transform_out="s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;"
                         data-start="1300"><p>Welcome to the number one money making platform.</p>
                    </div>
                    <div class="tp-caption  tp-resizeme"
                         data-x="['left','left','left','center']" data-hoffset="['0','15','15','15']"
                         data-y="['405','305','280','175']" data-voffset="['0','0','0','0']"
                         data-responsive_offset="on"
                         data-transform_idle="o:1;"
                         data-transform_in="y:-50px;opacity:0;s:1500;e:Power3.easeOut;"
                         data-transform_out="s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;"
                         data-start="1600">
                        <a href="{{ route('register') }}" class="btn-border text-uppercase border_radius">Start Now</a>
                        <a href="{{ route('login') }}" class="text-uppercase border_radius btn-green">Sign in</a>
                    </div>
                </li>
                <li data-transition="fade">
                    <img src="{{ asset('images/home4-banner2.jpg') }}"  alt="" data-bgposition="center center" data-bgfit="cover">
                    <h1 class="tp-caption  tp-resizeme uppercase"
                        data-x="['left','left','left','center']" data-hoffset="['0','15','15','15']"
                        data-y="['260','160','160','90']" data-voffset="['0','0','0','0']"
                        data-responsive_offset="on"
                        data-transform_idle="o:1;"
                        data-transform_in="y:-50px;opacity:0;s:1500;e:Power3.easeOut;"
                        data-transform_out="s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;"
                        data-start="1000">How Does <strong class="blue_t">It Work?</strong>
                    </h1>
                    <div class="tp-caption  tp-resizeme"
                         data-x="['left','left','left','center']" data-hoffset="['0','15','15','15']"
                         data-y="['330','230','230','170']" data-voffset="['0','0','0','0']"
                         data-responsive_offset="on"
                         data-transform_idle="o:1;"
                         data-transform_in="y:-50px;opacity:0;s:1500;e:Power3.easeOut;"
                         data-transform_out="s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;"
                         data-start="1400"><p>To enter the money making machine, register now and invite others</p>
                    </div>
                    <div class="tp-caption  tp-resizeme"
                         data-x="['left','left','left','center']" data-hoffset="['0','15','15','15']"
                         data-y="['405','305','280','175']" data-voffset="['0','0','0','0']"
                         data-responsive_offset="on"
                         data-transform_idle="o:1;"
                         data-transform_in="y:-50px;opacity:0;s:1500;e:Power3.easeOut;"
                         data-transform_out="s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;"
                         data-start="1700">
                        <a href="{{ route('register') }}" class="btn-border text-uppercase border_radius">Start Now</a>
                        <a href="{{ route('login') }}" class="text-uppercase border_radius btn-green">Sign in</a>
                    </div>
                </li>
            </ul>
        </div>
    </section>
    <!--Slider ends-->

    <!--Video-->
    <section id="bg-video" class="padding">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 video wow">
                    <iframe src="https://drive.google.com/file/d/0B5nlUW7I3fsYYzI3SDNlcXRKRHc/preview" class="col-md-12" height="350"></iframe>
                </div>
                <div class="col-sm-6 right_content bottom40 wow fadeInRight">
                    <h2 class="bottom30 text-capitalize">Who <span class="green_t">We Are</span></h2>
                    <p class="bottom30">
	                    We are a group of people who came together to form a leverage system (Osusu) it is the cooperation of friends and people who believe they could help themselves through contributions made to each other by themselves and to themselves, thereby making money available for themselves with the sole purpose of lifting their financial ability for their businesses and personal life enhancement.
                    </p>
                    <p class="bottom30">
	                    This has worked perfectly for us over the years without any problems,
	                    we boost our business financially through this system. You earn money to increase the capital base for your business and to meet your personal needs as well.
                    </p>
	                <p class="bottom30">
		                This has been working for us so we thought it wise to help others to a financial freedom. Hence decide to bring  WEALTH TO YOU. This is about you becoming a millionaire through cooperation between people of like mind.
	                </p>
                    <a href="{{ route('register') }}" class="btn-white text-uppercase border_radius">get started</a>
                </div>
            </div>
        </div>
    </section>
    <!--Video-->
    @if(count($testimonials))
    <section id="carousel">
        <div class="container">
            <h1>WHAT OTHERS ARE SAYING</h1>
            <div class="row">
                <div class="col-md-12">
                    <div class="carousel slide" data-ride="carousel" id="quote-carousel">

                        <!-- Bottom Carousel Indicators -->
                        <ol class="carousel-indicators">
                            @for($i=0; $i<count($testimonials); $i++)
                            <li data-target="#quote-carousel" data-slide-to="{{ $i }}" class="{{ $i==0?"active":"" }}"></li>
                            @endfor
                        </ol>

                        <!-- Carousel Slides / Quotes -->
                        <div class="carousel-inner">
                            @php
                                $i=0;
                            @endphp
                            @foreach($testimonials as $testimonial)
                            <!-- Quote 1 -->
                            <div class="item {{ $i==0?"active":"" }}">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <p>&ldquo;{{ $testimonial->body }}&rdquo;</p>
                                        <small><strong>{{ $testimonial->user->name }}</strong></small>
                                    </div>
                                </div>
                            </div>@php $i++ @endphp
                            @endforeach
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
    @endif
@endsection

@section('bottom')
    <script>
        $(document).ready(function() {
            //carousel options
            $('#quote-carousel').carousel({
                pause: true, interval: 10000
            });
        });
    </script>
@endsection

