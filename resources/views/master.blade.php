<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
<title>Welcome To Wealth To You</title>
<link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/font-awesome.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/envas-icons.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/animate.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/cubeportfolio.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/owl.carousel.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/owl.transitions.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/settings.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/bootsnav.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/loader.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/style.css') }}">
<link rel="icon" href="{{ asset('images/favicon2.png') }}">
<script src="https://use.fontawesome.com/b623e542b5.js"></script>
</head>

<body>

<!--Loader-->
<div class="loader">
  <div class="cssload-loader">
    <div class="cssload-inner cssload-one"></div>
    <div class="cssload-inner cssload-two"></div>
    <div class="cssload-inner cssload-three"></div>
  </div>
</div>
<!--Loader Ends -->

@include('partials.header')

@yield('content')

<div class="copyright">
  <div class="container">
    <div class="row">
      <div class="col-md-12 text-center">
        <p>Copyright &copy; {{ date("Y") }} <a href="{{ route('welcome') }}">Wealth To You</a>. All rights reserved.</p>
      </div>
    </div>
  </div>
</div>
<!--Footer ends-->

<script src="{{ asset('js/jquery-2.2.3.js') }}"></script>
<script src="{{ asset('js/bootstrap.min.js') }}"></script>
<script src="{{ asset('js/jquery.parallax-1.1.3.js') }}"></script>
<script src="{{ asset('js/jquery.appear.js') }}"></script>
<script src="{{ asset('js/jquery-countTo.js') }}"></script>
<script src="{{ asset('js/bootsnav.js') }}"></script>
<script src="{{ asset('js/jquery.cubeportfolio.min.js') }}"></script>
<script src="{{ asset('js/owl.carousel.min.js') }}"></script>
<script src="{{ asset('js/viedobox_video.js') }}"></script>
<script src="{{ asset('js/jquery.themepunch.tools.min.js') }}"></script>
<script src="{{ asset('js/jquery.themepunch.revolution.min.js') }}"></script>
<script src="{{ asset('js/jquery.themepunch.revolution.min.js') }}"></script>
<script src="{{ asset('js/revolution.extension.layeranimation.min.js') }}"></script>
<script src="{{ asset('js/revolution.extension.navigation.min.js') }}"></script>
<script src="{{ asset('js/revolution.extension.parallax.min.js') }}"></script>
<script src="{{ asset('js/revolution.extension.slideanims.min.js') }}"></script>
<script src="{{ asset('js/revolution.extension.video.min.js') }}"></script>
<script src="{{ asset('js/wow.min.js') }}"></script>
<script src="{{ asset('js/functions.js') }}"></script>
<script>
    function logOut(event) {
        event.preventDefault();
        document.getElementById('logout-form').submit();
    }
</script>
@yield('bottom')
</body>
</html>
