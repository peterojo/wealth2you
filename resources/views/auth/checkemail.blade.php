@extends('layouts.template')

@section('page-title')
    Check Email
@endsection

@section('content')
    <div class="text-center">
        <a href="/" class="logo"><span>Wealth<span>toYou</span></span></a>
    </div>
    <div class="m-t-40 card-box">
        <div class="text-center">
            <h4 class="text-uppercase font-bold m-b-0">Registered!</h4>
        </div>
        <div class="panel-body">
	        <div class="alert alert-success">
		        You have been registered successfully! <strong>Check your email</strong> for confirmation link to enable your account.
	        </div>
        </div>
    </div>
    <!-- end card-box-->

    <div class="row">
        <div class="col-sm-12 text-center">
            <p class="text-muted">Don't have an account? <a href="{{ route('register') }}" class="text-primary m-l-5"><b>Register</b></a> to start earning cash now</p>
        </div>
    </div>
@endsection