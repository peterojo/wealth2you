@extends('layouts.template')

@section('page-title') Register @endsection

@section('styles')
    @parent
    <link rel="stylesheet" href="{{ asset('assets/plugins/intl-tel-input/build/css/intlTelInput.css') }}">
    <style type="text/css">
        .iti-flag {background-image: url("{{ asset('assets/plugins/intl-tel-input/build/img/flags.png') }}");}
        .intl-tel-input {
            display: block;
            clear: both;
        }
        @media only screen and (-webkit-min-device-pixel-ratio: 2), only screen and (min--moz-device-pixel-ratio: 2), only screen and (-o-min-device-pixel-ratio: 2 / 1), only screen and (min-device-pixel-ratio: 2), only screen and (min-resolution: 192dpi), only screen and (min-resolution: 2dppx) {background-image: url("assets/plugins/intl-tel-input/build/img/flags@2x.png");}
    </style>
@stop

@section('content')
    <div class="text-center">
        <a href="/" class="logo"><span>Wealth<span>toYou</span></span></a>
        <h5 class="text-muted m-t-0 font-600">Please complete the form below</h5>
    </div>
    <div class="m-t-40 card-box">
        <div class="text-center">
            <h4 class="text-uppercase font-bold m-b-0">Register</h4>
        </div>
        <div class="panel-body">
            @include('partials.flash')
            <form class="form-horizontal m-t-20" action="{{ url('register') }}" method="post">
                {{ csrf_field() }}
                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                    <div class="col-xs-12">
                        <input class="form-control" type="text" name="name" required placeholder="Full Name" value="{{ old('name') }}">
                    </div>
                </div>
                @if ($errors->has('name'))
                    <div class="alert alert-danger alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        {{ $errors->first('name') }}
                    </div>
                @endif
                @if(!$referrer)
                    <div class="form-group{{ $errors->has('currency') ? ' has-error' : '' }}">
                        <div class="col-sm-12">
                            <select name="currency" class="form-control">
                                <option value="">Select Currency</option>
                                <option value="NGN"{{ old('currency')=='NGN'?' selected':'' }}>Naira</option>
                                <option value="USD"{{ old('currency')=='USD'?' selected':'' }}>Dollar</option>
                            </select>
                        </div>
                    </div>
                    @if ($errors->has('currency'))
                        <div class="alert alert-danger alert-dismissible" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            {{ $errors->first('currency') }}
                        </div>
                    @endif
                    <div class="form-group{{ $errors->has('amount') ? ' has-error' : '' }}">
                        <div class="col-sm-12">
                            <select name="amount" class="form-control">
                                <option value="">Select Amount</option>
                                <option value="1000"{{ old('amount')=='1000'?' selected':'' }}>1,000</option>
                                <option value="5000"{{ old('amount')=='5000'?' selected':'' }}>5,000</option>
                                <option value="10000"{{ old('amount')=='10000'?' selected':'' }}>10,000</option>
                                <option value="20000"{{ old('amount')=='20000'?' selected':'' }}>20,000</option>
                            </select>
                        </div>
                    </div>
                    @if ($errors->has('amount'))
                        <div class="alert alert-danger alert-dismissible" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            {{ $errors->first('amount') }}
                        </div>
                    @endif
                @endif
                <div class="form-group{{ $errors->has('phone_number') ? ' has-error' : '' }}">
                    <div class="col-xs-12">
                        <input class="form-control" type="text" id="phone_number" name="phone_number" required placeholder="Phone Number" value="{{ old('h_phone') }}">
                    </div>
                    <input type="hidden" id="h_phone" name="h_phone" value="{{ old('h_phone') }}">
                </div>
                @if ($errors->has('phone_number'))
                    <div class="alert alert-danger alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        {{ $errors->first('phone_number') }}
                    </div>
                @endif
                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    <div class="col-xs-12">
                        <input class="form-control" type="email" name="email" required placeholder="Email" value="{{ old('email') }}">
                    </div>
                </div>
                @if ($errors->has('email'))
                    <div class="alert alert-danger alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        {{ $errors->first('email') }}
                    </div>
                @endif
                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                    <div class="col-xs-12">
                        <input class="form-control" type="password" name="password" required placeholder="Password">
                    </div>
                </div>
                @if ($errors->has('password'))
                    <div class="alert alert-danger alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        {{ $errors->first('password') }}
                    </div>
                @endif
                <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                    <div class="col-xs-12">
                        <input class="form-control" type="password" name="password_confirmation" required placeholder="Password Confirmation">
                    </div>
                </div>
                @if ($errors->has('password_confirmation'))
                    <div class="alert alert-danger alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        {{ $errors->first('password_confirmation') }}
                    </div>
                @endif
                <div class="form-group{{ $errors->has('terms') ? ' has-error' : '' }}">
                    <div class="col-xs-12">
                        <div class="checkbox checkbox-custom">
                            <input id="checkbox-signup" name="terms" type="checkbox" checked="checked">
                            <label for="checkbox-signup">I accept <a href="{{ route('about') }}#terms" target="_blank">Terms and Conditions</a></label>
                        </div>
                    </div>
                </div>
                @if ($errors->has('terms'))
                    <div class="alert alert-danger alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        {{ $errors->first('terms') }}
                    </div>
                @endif
                <div class="form-group text-center m-t-40">
                    <div class="col-xs-12">
                        <button class="btn btn-custom btn-bordred btn-block waves-effect waves-light" type="submit">
                            Register
                        </button>
                    </div>
                </div>
	            @if(Request::has('inv'))
		            <input type="hidden" name="inv" value="{{ Request::get('inv') }}">
				@endif
                @if($referrer)
                    <p><strong>Referrer:</strong> {{ $referrer->owner->name }}</p>
                    <input type="hidden" name="ref_cert_id" value="{{ $referrer->id }}">
                @endif
            </form>

        </div>
    </div>
    <!-- end card-box -->

    <div class="row">
        <div class="col-sm-12 text-center">
            <p class="text-muted">Already have account? <a href="{{ route('login') }}" class="text-primary m-l-5"><b>Log In</b></a>
            </p>
        </div>
    </div>
	
	<div class="well">
		<h5>DISCLAIMER!</h5>
		<small>
			Registration does not guarantee that you will start receiving confirmations, earning or reward of any kind.
			Your earning is based on your ability to refer your three (3) persons and coordinate your downline/team,
			THERE IS NO REFUND OF MONEY AFTER CONTRIBUTION  ARE MADE because contributions are made directly into members' accounts of which w2u have no access.
		</small>
	</div>
@endsection

@section('scripts')
    @parent
    <script src="{{ asset('assets/plugins/intl-tel-input/build/js/intlTelInput.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/intl-tel-input/build/js/utils.js') }}"></script>
    <script>
        $('#phone_number').on('blur', function() {
        $('#h_phone').val($('#phone_number').intlTelInput("getNumber"));
        });
        $("#phone_number").intlTelInput({
            utilsScript: "{{ asset('assets/plugins/intl-tel-input/build/js/utils.js') }}",
            autoHideDialCode: false,
            preferredCountries : [],
            initialCountry: 'auto',
            separateDialCode: true,
            customPlaceholder: function(selectedCountryPlaceholder, selectedCountryData) {
                return "e.g. " + selectedCountryPlaceholder;
            },
            geoIpLookup: function(callback) {
                $.get("https://ipinfo.io", function() {}, "jsonp").always(function(resp) {
                    var countryCode = (resp && resp.country) ? resp.country : "";
                    callback(countryCode);
                });
            }
        });
    </script>
@stop
