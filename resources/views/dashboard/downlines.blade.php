@extends('dashboard.master')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="card-box">
                    <h4 class="header-title m-t-0 m-b-30">{{ auth()->id() == $user->id ? "Your " : $user->name . "'s " }}Direct Downlines</h4>

                    <p class="text-muted font-13 m-b-25">

                    </p>

                    <div class="table-responsive">
                        <table class="table m-0">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Number of Downlines</th>
                                <th>Phone Number</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @php $i=1; @endphp
                            @forelse($certificates as $certificate)
                            <tr>
                                <th scope="row">{{ $i++ }}</th>
                                <td>{{ $certificate->assignee->name }}</td>
                                <td>{{ $certificate->assignee->assignedCertificates()->count() }}</td>
                                <td>{{ $certificate->assignee->phone }}</td>
                                <td>
                                    <a href="{{ route('downlines', $certificate->assignee->code) }}" class="btn btn-info waves-effect">
                                        View Downlines
                                    </a>
                                </td>
                            </tr>
                            @empty
                            <tr>
                                <td colspan="5" style="text-align: center">No downlines</td>
                            </tr>
                            @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div><!-- end col -->

        </div>
        <!-- end row -->
    </div> <!-- container -->
@endsection