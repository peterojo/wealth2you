@extends('dashboard.master')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                @include('partials.flash')
                <form action="{{ route('testimonial.new') }}" method="post" role="form">
                    <div class="form-group">
                        <label for="title">Title</label>
                        <input type="text" class="form-control" name="title" id="title">
                        @if($errors->has('title'))
                            <div class="alert alert-danger">
                                <button type="button" class="close" data-dismiss="alert"
                                        aria-hidden="true">&times;</button>
                                {{ $errors->first('title') }}
                            </div>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="body">Body</label>
                        <textarea name="body" id="body" class="form-control"></textarea>
                        @if($errors->has('body'))
                            <div class="alert alert-danger">
                                <button type="button" class="close" data-dismiss="alert"
                                        aria-hidden="true">&times;</button>
                                {{ $errors->first('body') }}
                            </div>
                        @endif
                    </div>
                    {{ csrf_field() }}
                    <button type="submit" class="btn btn-primary"><i class="fa fa-send"></i>&nbsp;Send</button>
                </form>
                <hr>
                @forelse($testimonials as $testimonial)
                    <h4 class="header-title m-t-20 m-b-30 text-uppercase">{{ $testimonial->title }}</h4>

                    <div class="card-box">
                        <small class="text-muted pull-left">{{ $testimonial->user->id===auth()->id() ? "You" : "Admin" }} said:</small>
                        <small class="text-muted pull-right">{{ $testimonial->created_at->diffForHumans() }}</small>
                        <hr>
                        <p class="font-13 m-b-25">
                            {{ $testimonial->body }}
                        </p>
                    </div>

                @empty
                    <div class="text-center m-t-20 bg-warning">You have not created any testimonial yet.</div>
                @endforelse
            </div><!-- end col -->
        </div>
        <!-- end row -->
    </div> <!-- container -->
@endsection