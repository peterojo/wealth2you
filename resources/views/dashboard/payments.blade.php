@extends('dashboard.master')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                @include('partials.flash')
                <div class="card-box">
                    <h4 class="header-title m-t-0 m-b-30">Manage Incoming Contributions</h4>

                    <p class="text-muted font-13 m-b-25">

                    </p>

                    <div class="table-responsive">
                        <table class="table m-0">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Contributor Name</th>
                                <th>Email</th>
                                <th>Phone Number</th>
                                <th>Payment Status</th>
	                            <th>Proof</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @php $i=1; @endphp
                            @forelse($contributions as $contribution)
                            <tr>
                                <th scope="row">{{ $i++ }}</th>
                                <td>{{ $contribution->user->name }}</td>
                                <td>{{ $contribution->user->email }}</td>
                                <td>{{ $contribution->user->phone }}</td>
                                <td>
                                    @if($contribution->isConfirmed())
                                    <span class="label label-success" style="padding: 5px 20px;">Confirmed</span>
                                    @elseif($contribution->isAwaitingConfirmation())
                                    <span class="label label-warning" style="padding: 5px 20px;">Awaiting Confirmation</span>
                                    @else
                                    <span class="label label-danger" style="padding: 5px 20px;">Unpaid</span>
                                    @endif
                                </td>
	                            <td>
		                            @if($contribution->proof)
			                            <a href="/storage/{{ $contribution->proof }}" target="_blank">View Upload</a>
		                            @else
		                                Not uploaded
		                            @endif
	                            </td>
                                <td>
                                    @if($contribution->isAwaitingConfirmation())
                                        <form action="{{ route('contribute.confirm', $contribution->trans_id) }}" method="post">
                                            {{ csrf_field() }}
                                            <button type="submit" class="btn btn-info waves-effect">
                                                Confirm
                                            </button>
                                        </form>
                                    @endif
                                </td>
                            </tr>
                            @empty
                                <tr>
                                    <td colspan="6" style="text-align: center">No contributions</td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>
                        {{ $contributions->links() }}
                    </div>
                </div>
            </div><!-- end col -->

        </div>
        <!-- end row -->
    </div> <!-- container -->
@endsection