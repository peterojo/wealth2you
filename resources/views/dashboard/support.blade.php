@extends('dashboard.master')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <a data-toggle="modal" href="#contact-support" class="btn btn-success btn-block"><i class="fa fa-plus"></i>&nbsp;&nbsp;New Ticket</a>
                <p>&nbsp;</p>
                <div class="list-group">
                    @foreach($my_tickets as $my_ticket)
                    <a href="{{ route('support', $my_ticket->ticket_code) }}" class="list-group-item{{ ($my_ticket->id==$ticket->id)?" active":"" }}">
                        {{ $my_ticket->subject }}
                    </a>
                    @endforeach
                </div>
            </div>
            <div class="col-md-8">
                @include('partials.flash')
                @if($ticket)
                    <h4 class="header-title m-t-0 m-b-30 text-uppercase">{{ $ticket->subject }}</h4>

                    @foreach($ticket->messages->reverse() as $message)
                    <div class="card-box">
                        <small class="text-muted pull-left">{{ $message->user->id===auth()->id() ? "You" : "Admin" }} said:</small>
                        <small class="text-muted pull-right">{{ $message->created_at->diffForHumans() }}</small>
                        <hr>
                        <p class="font-13 m-b-25">
                            {{ $message->message }}
                        </p>
                    </div>
                    @endforeach
                    <form action="{{ route('ticket.store-message', $ticket->ticket_code) }}" method="post" role="form">
                        <div class="form-group">
                            <textarea name="message" id="body" class="form-control"></textarea>
                            @if($errors->has('message'))
                                <div class="alert alert-danger">
                                    <button type="button" class="close" data-dismiss="alert"
                                            aria-hidden="true">&times;</button>
                                    {{ $errors->first('message') }}
                                </div>
                            @endif
                        </div>
                        {{ csrf_field() }}
                        <button type="submit" class="btn btn-primary"><i class="fa fa-send"></i>&nbsp;Send</button>
                    </form>
                @else
                    <p class="text-center bg-warning">You have not created any support ticket.</p>
                @endif
            </div><!-- end col -->
        </div>
        <!-- end row -->
    </div> <!-- container -->

    <div class="modal fade" id="contact-support">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Contact Wealth2You Customer Support</h4>
                </div>
                <form action="{{ route('ticket.store') }}" method="post" role="form">
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="trans_id">Transaction</label>
                            <select name="trans_id" id="trans_id" class="form-control">
                                <option value="">Select</option>
                                @foreach(auth()->user()->benefits as $transaction)
                                    <option value="{{ $transaction->trans_id }}">{{ $transaction->currency." ".number_format($transaction->amount)." from ".$transaction->user->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="subject">Subject</label>
                            <input type="text" class="form-control" name="subject" id="subject" required>
                        </div>
                        <div class="form-group">
                            <label for="body">Message</label>
                            <textarea name="message" id="body" class="form-control" required></textarea>
                        </div>
                    </div>
                    <div class="modal-footer">
                        {{ csrf_field() }}
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Send</button>
                    </div>
                </form>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
@endsection