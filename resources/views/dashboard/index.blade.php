@extends('dashboard.master')

@section('page-title') Dashboard @endsection

@section('styles')
    @parent
    <!--Morris Chart CSS -->
    <link rel="stylesheet" href="{{ asset('assets/plugins/morris/morris.css') }}">
@endsection

@section('content')
    <div class="container">
        <div class="row">
            @include('partials.flash')
            <div class="col-md-3">
                <div class="card-box">
                    <h4 class="header-title m-t-0 m-b-30">Highest Earning Level</h4>

                    <div class="widget-chart-1">
                        <div class="">
                            <h2 class="m-b-0">{{ $highest_level }} </h2>
                            <p class="text-muted">Level</p>
                        </div>
                    </div>
                </div>
            </div><!-- end col -->

            <div class="col-md-3">
                <div class="card-box">
                    <h4 class="header-title m-t-0 m-b-30">Downlines</h4>

                    <div class="widget-chart-1">
                        <div class="">
                            <h2 class="m-b-0"> {{ $num_downlines }} </h2>
                            <p class="text-muted">{{ str_plural("Downline", $num_downlines) }}</p>
                        </div>
                    </div>
                </div>
            </div><!-- end col -->

            <div class="col-md-3">
                <div class="card-box">
                    <h4 class="header-title m-t-0 m-b-30">Total Earnings</h4>

                    <div class="widget-chart-1">
                        <div class="">
                            <h2 class="m-b-0"> {{ number_format($earned) }} </h2>
                            <p class="text-muted">{{ auth()->user()->package->currency }}</p>
                        </div>
                    </div>
                </div>
            </div><!-- end col -->

            <div class="col-md-3">
                <div class="card-box">
                    <h4 class="header-title m-t-0 m-b-30">Total Contributions</h4>

                    <div class="widget-box-2">
                        <h2 class="m-b-0"> {{ number_format($paid) }} </h2>
                        <p class="text-muted">{{ auth()->user()->package->currency }}</p>
                    </div>
                </div>
            </div><!-- end col -->

        </div>
        <!-- end row -->

        <div class="row">
            @foreach(auth()->user()->certificates()->with('assignee')->get() as $certificate)
                <div class="col-lg-4">
                @if($certificate->isTaken())
                    <div class="card-box">
                        <span class="label label-success pull-right"><i class="fa fa-check"></i></span>
                        <h4 class="header-title m-t-0 m-b-30">Assigned</h4>

                        <div class="inbox-widget nicescroll" style="max-height: 315px;">
                            <a href="{{ route('downlines', $certificate->assignee->code) }}">
                                <div class="inbox-item">
                                    <div class="inbox-item-img"><img
                                                src="{{ asset('assets/images/users/avatar-1.jpg') }}"
                                                class="img-circle" alt=""></div>
                                    <p class="inbox-item-author">{{ $certificate->assignee->name }}</p>
                                    <p class="inbox-item-text">{{ $certificate->assignee->phone }}</p>
                                </div>
                            </a>
                        </div>
                    </div>
                @else
	                <div class="card-box">
	                    <div class="dropdown pull-right">
	                        <a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown"
	                           aria-expanded="false">
	                            <i class="zmdi zmdi-more-vert"></i>
	                        </a>
	                        <ul class="dropdown-menu" role="menu">
	                            @if($certificate->isUpForGrabs())
	                                <li>
	                                    <a data-toggle="modal" href="#stop-auto-assign">Revoke from Auto Assign</a>
	                                </li>
	                            @else
	                                <li><a href="#">Assign to Downline</a></li>
	                                <li><a data-toggle="modal" href="#auto-assign">Queue for Auto Assign</a></li>
	                            @endif
	                        </ul>
	                    </div>
	                    <h4 class="header-title m-t-0 m-b-30">Certificate code: {{ $certificate->code }}</h4>

	                    <div class="inbox-widget nicescroll" style="max-height: 315px;">
	                        @foreach($certificate->paymentsAccountListing() as $account)
	                            <div class="inbox-item">
	                                <div class="inbox-item-img"><img
	                                            src="{{ asset('assets/images/users/avatar-1.jpg') }}"
	                                            class="img-circle" alt=""></div>
	                                <p class="inbox-item-author">{{ $account->name }}</p>
	                                <p class="inbox-item-text">{{ $account->bank }} {{ $account->number }}</p>

	                            </div>
	                        @endforeach
	                    </div>
	                </div>
                    @if($certificate->isUpForGrabs())
                    <a data-toggle="modal" href="#stop-auto-assign" class="btn btn-danger waves-effect">Revoke from Automatic Assignment</a>
                    <div class="modal fade" id="stop-auto-assign">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal"
                                            aria-hidden="true">&times;</button>
                                    <h4 class="modal-title">Revoke Automatic Certificate Assignment</h4>
                                </div>
                                <div class="modal-body">
                                    <p>You are about to revoke this certificate from automatic assignment.</p>
                                    <p>This means that your certificate will no longer be queued for automatic assignment.</p>
                                    <p>Now you have to assign this certificate to someone manually</p>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel
                                    </button>
                                    <form action="{{ route('auto-assign', [$certificate->code, '0']) }}" class="form-inline" method="post">
                                        {{ csrf_field() }}
                                        <button type="submit" class="btn btn-primary">Revoke</button>
                                    </form>
                                </div>
                            </div><!-- /.modal-content -->
                        </div><!-- /.modal-dialog -->
                    </div><!-- /.modal -->
                    @else
                    <a href="#assign" data-toggle="modal" class="btn btn-primary waves-effect waves-light m-b-5">
                        Assign Certificate to Downline
                    </a>
                    <div class="modal fade" id="assign">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal"
                                            aria-hidden="true">&times;</button>
                                    <h4 class="modal-title">Assign this Certificate to a Downline</h4>
                                </div>
                                <div class="modal-body">
                                    <p>You can copy the unique link below and send it to the assignee to register</p>
                                    <label for="regLink">Registration Link:</label>
                                    <div class="input-group">
                                        <input type="text"
                                               id="regLink"
                                               class="form-control"
                                               value="{{ route('register') }}?ref={{ $certificate->code }}"
                                               onfocus="this.select()">
                                        <span class="input-group-btn">
                                            <button class="btn btn-default" id="cpBtn" data-clipboard-target="#regLink" type="button">Copy</button>
                                        </span>
                                    </div><!-- /input-group -->
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel
                                    </button>
                                    <button type="button" data-dismiss="modal" class="btn btn-primary">OK</button>
                                </div>
                            </div><!-- /.modal-content -->
                        </div><!-- /.modal-dialog -->
                    </div><!-- /.modal -->
                    <a data-toggle="modal" href="#auto-assign" class="btn btn-warning waves-effect">Queue for Automatic Assignment</a>
                    <div class="modal fade" id="auto-assign">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal"
                                            aria-hidden="true">&times;</button>
                                    <h4 class="modal-title">Automatic Certificate Assignment</h4>
                                </div>
                                <div class="modal-body">
                                    <p>You are about to queue this certificate for automatic assignment.</p>
                                    <p>This means that a random new user that matches your payment package will be assigned to you</p>
                                    <p>This process may take a while.</p>
                                    <p>If you would like to assign this certificate to someone you know instead, then click 'Cancel'</p>
                                    <p>Otherwise, click 'Proceed'</p>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel
                                    </button>
                                    <form action="{{ route('auto-assign', [$certificate->code, '1']) }}" class="form-inline" method="post">
                                        {{ csrf_field() }}
                                        <button type="submit" class="btn btn-primary">Proceed</button>
                                    </form>
                                </div>
                            </div><!-- /.modal-content -->
                        </div><!-- /.modal-dialog -->
                    </div><!-- /.modal -->
                    @endif
                @endif
                </div><!-- end col -->
            @endforeach
        </div>
        <!-- end row -->
    </div> <!-- container -->
@endsection

@section('scripts')
    @parent
    <script src="https://cdnjs.cloudflare.com/ajax/libs/clipboard.js/1.6.1/clipboard.min.js"></script>
    <script>
        new Clipboard('#cpBtn');
    </script>
@stop