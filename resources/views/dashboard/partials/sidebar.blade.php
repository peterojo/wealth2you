<!-- ========== Left Sidebar Start ========== -->
<div class="left side-menu">
    <div class="sidebar-inner slimscrollleft">

        <!-- User -->
        <div class="user-box">
            {{--<div class="user-img">
                <img src="{{ asset('assets/images/users/avatar-1.jpg') }}" alt="user-img" title="Mat Helme"
                     class="img-circle img-thumbnail img-responsive">
                <div class="user-status offline"><i class="zmdi zmdi-dot-circle"></i></div>
            </div>--}}
            <h5><a href="#">{{ auth()->user()->name }}</a></h5>
            <ul class="list-inline">
                <li>
                    <a href="#">
                        <i class="zmdi zmdi-settings"></i>
                    </a>
                </li>

                <li>
                    <a href="{{ route('logout') }}" class="text-custom"
                       onclick="event.preventDefault();
                               document.getElementById('logout-form').submit();">
                        <i class="zmdi zmdi-power"></i>
                    </a>
                </li>
            </ul>
        </div>
        <!-- End User -->

        <!--- Sidemenu -->
        <div id="sidebar-menu">
            <ul>
                <li class="text-muted menu-title">Navigation</li>

                <li>
                    <a href="{{ url('dashboard') }}" class="waves-effect active"><i class="fa fa-desktop"></i>
                        <span> Dashboard </span> </a>
                </li>

                <li>
                    <a href="{{ route('welcome') }}" class="waves-effect"><i class="zmdi zmdi-view-dashboard"></i>
                        <span> Home </span> </a>
                </li>
                <li>
                    <a href="{{ route('downlines') }}" class="waves-effect"><i class="fa fa-users"></i>
                        <span> Direct Downlines </span> </a>
                </li>
                <li>
                    <a href="{{ route('contributions') }}" class="waves-effect"><i class="fa fa-credit-card-alt"></i> <span> Manage Contributions </span>
                    </a>
                </li>
                <li>
                    <a href="{{ route('invite') }}" class="waves-effect"><i class="fa fa-bullhorn"></i>
                        <span> Invite Contacts </span> </a>
                </li>
                <li>
                    <a href="{{ route('testimonial.create') }}" class="waves-effect"><i class="fa fa-pencil-square-o"></i>
                        <span> Create Testimonial </span>
                    </a>
                </li>
                <li>
                    <a href="{{ route('support') }}" class="waves-effect"><i class="fa fa-envelope"></i>
                        <span> Contact Support </span>
                    </a>
                </li>
                @if(auth()->user()->isAdmin())
                <li class="divider"></li>
                <li>
                    <a href="{{ route('admin.registrations') }}">ADMIN: Manage Registrations</a>
                </li>
                <li>
                    <a href="{{ route('admin.testimonials') }}">ADMIN: Manage Testimonials</a>
                </li>
                <li class="divider"></li>
                @endif
                <li>
                    <a href="{{ route('logout') }}"
                       class="waves-effect"
                       onclick="event.preventDefault();
                               document.getElementById('logout-form').submit();">
                        <i class="zmdi zmdi-power"></i>
                        <span>Logout </span> </a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
                </li>
            </ul>
            <div class="clearfix"></div>
        </div>
        <!-- Sidebar -->
        <div class="clearfix"></div>

    </div>

</div>
<!-- Left Sidebar End -->