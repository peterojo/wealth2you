@extends('master')

@section('content')
    <!--About Us-->
    <section id="about" class="padding-top">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 padding-bottom"> <img src="{{ asset('images/about.jpg') }}" alt="About Us" class="img-responsive"> </div>
                <div class="col-sm-6 about_right padding-bottom">
                    <h2 class="bottom10">How It <span class="blue_t">Works</span></h2>
	                <p class="bottom30">
		                By becoming our member, you automatically place yourself in a in a chain where people from all over the country supports each other by paying thousands of naira into your account everyday. The goal of this system is to help people put little efforts but yet make a lot of money and this will guarantee us all to an end point – SUCCESS.
	                </p>
	                <p class="bottom30">
		                We have over 170 million citizens in Nigeria. Just think about it, if all those people give N10 each, you’ll become a millionaire automatically. But how do you connect to all of these people. Becoming a member of Wealth to You gives you that connection.
	                </p>
	                <p class="bottom30">
		                At Wealth To You, we follow on course until success and that “Focus”.
		                To become a member of the W2U system is simple. Get an invitation certificate from someone who’s already a member.
	                </p>
                </div>
            </div>
        </div>
    </section>

    <!--Quality Fine-->
    <section id="quality" class="padding">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 text-center">
                    <h2>Start Earning Money in 3 Easy Steps</h2>
                </div>
                <div class="clearfix"></div>
                <div class="quality_wrap clearfix">
                    <div class="col-md-6">
                        <div class="left"> <img src="{{ asset('images/quality.jpg') }}" alt="quality"> </div>
                    </div>
                    <div class="col-md-6">
                        <div class="right">
                            <div class="media">
                                <div class="media-left">
                                    <div class="media-object"><span>1</span></div>
                                </div>
                                <div class="media-body">
                                    <h4 class="bottom10">Register</h4>
                                    <p class="bottom15">
	                                    Pay N3,000 into the company’s account. This is for registration and administrative use.
                                    </p>
                                </div>
                            </div>
                            <div class="media">
                                <div class="media-left">
                                    <div class="media-object"><span>2</span></div>
                                </div>
                                <div class="media-body">
                                    <h4 class="bottom10">Contribute</h4>
                                    <p class="bottom15">
	                                    Contribute N1000 each into the account of 8 members whose names and account details show in your certificate.
                                    </p>
                                </div>
                            </div>
                            <div class="media">
                                <div class="media-left">
                                    <div class="media-object"><span>3</span></div>
                                </div>
                                <div class="media-body">
                                    <h4 class="bottom10">Upload</h4>
                                    <p>
	                                    Upload proof of payment and register your payment on the platform.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--Quality fine ends-->
    <section id="about3" class="padding-top">
	    <div class="container">
		    <div class="row clearfix">
			    <div class="col-sm-6 padding-bottom">
				    <img src="{{ asset('images/about.jpg') }}" alt="About Us" class="img-responsive">
			    </div>
			    <div class="col-sm-6 about_right padding-bottom">
				    <h2 class="bottom10">Understanding the <span class="blue_t">Contribution System</span></h2>
				    <p class="bottom30">
					    Wealth to you is about contribution popularly known as  Osusu in Nigeria. It is about joining a group of few or more persons who have agreed to contribute to each other certain amounts of money which is given to each person in turns as agreed. Wealth to you is the e-type of the Osusu whereby you first have to join the group by paying the amount you intend to contribute to others in the group. The amount you registered with will determine the group you join because you all in the group will contribute the same amount. You are grouped according to the amount you are contributing.
				    </p>
				    <p class="bottom30">
					    After your registration, you proceed to make your contribution to already existing members which are usually eight in number and not more. After which you are qualified for others to contribute to you.
					    If you successfully contribute to the eight people you will thereafter be assigned three personalized forms with which you are to invite three person for you to start earning from contribution.
				    </p>
				    <p class="bottom30">
					    The system will help you get your downline populated these may take a long time but for quick earning you are required to introduce your own people with your three form by publicising your link on your social. media pages and sending a. Customized mail or SMSs to your contacts inviting them to your Page.
				    </p>
			    </div>
		    </div>
		    {{--<div class="row clearfix">
			    <div class="col-sm-6 padding-bottom">
				    <img src="{{ asset('images/about.jpg') }}" alt="About Us" class="img-responsive">
			    </div>
			    <div class="col-sm-6 about_right padding-bottom">
				    <h2 class="bottom10"><span class="blue_t">How Much</span> Do I Start With?</h2>
				    <p class="bottom30">
					    With N11,000 you already on your way to becoming a millionaire. After the N11,000 you  are not expected to pay any other money. Isn’t that amazing! The W2U platform runs a transparent system that allows all members see how their money is coming in.
				    </p>
				    <p class="bottom30">
					    You are now a W2U member ready to build a new financial legacy.
				    </p>
				    <p class="bottom30">
					    After your registration, the platform will provide you with your own profile page with 3 certificates of your own with your name at the bottom line. You have moved into the Crystal Level.
				    </p>
				    <p class="bottom30">
					    With the three invitation certificates issued to you, you’ll invite three of your serious friends who will go through the steps above just like you did. Once they make payments, you’ll receive N3,000 paid by your friends into your account. After that, your friends (invitees) will other friends with the certificate given to them and then your name moves to the next level in the queue – on the BRONZE level. Then the names of your friends take your place at the Crystal level.
				    </p>
			    </div>
		    </div>--}}
		    <div class="row">
			    <div class="col-sm-12 padding-bottom">
				    <h2 class="bottom10">Our <span class="blue_t">Vision</span></h2>
				    <p class="bottom10">
					    Our vision is to:
				    </p>
				    <ul class="list-group">
					    <li class="list-group-item">Ensure financial empowerment for all members</li>
					    <li class="list-group-item">All members have easy access to funds for the business of their
						    dream
					    </li>
					    <li class="list-group-item">We believe this could stop the robbery and kidnapping if this people
						    will have access to funds which is their motives
					    </li>
					    <li class="list-group-item">We are to make millionaires out of every home</li>
					    <li class="list-group-item">To delete poverty from every household</li>
					    <li class="list-group-item">When you have money, joy will follow thereby bringing peace to this nation</li>
					    <li class="list-group-item">Since money is the answer to all things there will be stability in homes and families. Just think about the kind of joy that will flood your home as you start receiving money, every second alerts from your bank that is money falling into your account nearly every second.</li>
				    </ul>
			    </div>
		    </div>
		    <div class="row">
			    <div class="col-sm-12">
					<h2><span class="blue_t">W2U</span> Hierarchy</h2>
					<table class="table table-hover">
						<thead>
						<tr>
							{{--<th>S/N</th>--}}
							<th>W2U Levels</th>
							<th>Amount Receivable</th>
						</tr>
						</thead>
						<tbody>
						<tr>
							{{--<td>8</td>--}}
							<td>Zenith</td>
							<td>N 6,561,000</td>
						</tr>
						<tr>
							{{--<td>7</td>--}}
							<td>Platinum</td>
							<td>N 2,184,000</td>
						</tr>
						<tr>
							{{--<td>6</td>--}}
							<td>Gold</td>
							<td>N 729,000</td>
						</tr>
						<tr>
							{{--<td>5</td>--}}
							<td>Sapphire</td>
							<td>N 243,000</td>
						</tr>
						<tr>
							{{--<td>4</td>--}}
							<td>Ruby</td>
							<td>N 81,000</td>
						</tr>
						<tr>
							{{--<td>3</td>--}}
							<td>Silver</td>
							<td>N 27,000</td>
						</tr>
						<tr>
							{{--<td>2</td>--}}
							<td>Bronze</td>
							<td>N 9,000</td>
						</tr>
						<tr>
							{{--<td>1</td>--}}
							<td>Crystal</td>
							<td>N 3,000</td>
						</tr>
						</tbody>
					</table>
			    </div>
		    </div>
		    <div class="row" id="terms">
			    <div class="col-sm-12 top40">
				    <h2 class="top40">Terms and Conditions</h2>
				    <ul class="list-group">
					    <li class="list-group-item">The system strongly forbids any act or attempt to be dubious</li>
					    <li class="list-group-item">All imputed information's are presume correct or else you shall be
						    penalized or permanently blocked from membership
					    </li>
					    <li class="list-group-item">Ensure that all your data's are correctly computer as you fill in to register, the system works with the information you provided</li>
					    <li class="list-group-item">If your email address is incorrect, this may lead to a Challenge of not getting information from the system</li>
					    <li class="list-group-item">Also your bank account details must be correctly filled to avoid payment not received</li>
					    <li class="list-group-item">Be kind enough to quickly verify all payments on the go by avoiding unnecessary delays</li>
					    <li class="list-group-item">If you are sure of payment made and the person refuse to verify report to us through our ticket or support system with proof</li>
					    <li class="list-group-item">You are not allowed to use the same email address for more than once. If you want to reregister you have to use another email address and not the same one already used</li>
					    <li class="list-group-item">We can not alter any registration nor change any name so be sure of what details you want to fill in for registration</li>
					    <li class="list-group-item">All payments made are not refundable knowing that payments are made directly into members' accounts and we can't reach back to them for a refund. Your money will come back to you only by contributions made to your accounts</li>
					    <li class="list-group-item">Be convincingly sure before you act or else don't act</li>
				    </ul>
			    </div>
		    </div>
	    </div>
    </section>
@endsection

