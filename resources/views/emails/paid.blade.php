<div data-x-div-type="body"
     style="font-family: Avenir, Helvetica, sans-serif;box-sizing: border-box;background-color: #F2F4F6;color: #74787E;height: 100%;line-height: 1.4;margin: 0;width: 100% !important;-webkit-text-size-adjust: none">


    <table width="100%" cellpadding="0" cellspacing="0"
           style="font-family: Avenir, Helvetica, sans-serif;box-sizing: border-box;background-color: #f5f8fa;margin: 0;padding: 0;width: 100%;-premailer-cellpadding: 0;-premailer-cellspacing: 0;-premailer-width: 100%">
        <tbody>
        <tr>
            <td align="center" style="font-family: Avenir, Helvetica, sans-serif;box-sizing: border-box">
                <table width="100%" cellpadding="0" cellspacing="0"
                       style="font-family: Avenir, Helvetica, sans-serif;box-sizing: border-box;margin: 0;padding: 0;width: 100%;-premailer-cellpadding: 0;-premailer-cellspacing: 0;-premailer-width: 100%">
                    <tbody>
                    <tr>
                        <td style="font-family: Avenir, Helvetica, sans-serif;box-sizing: border-box;padding: 25px 0;text-align: center">
                            <a href="http://w2u.greatergrace.biz/"
                               style="font-family: Avenir, Helvetica, sans-serif;box-sizing: border-box;color: #bbbfc3;font-size: 19px;font-weight: bold;text-decoration: none;text-shadow: 0 1px 0 white"
                               target="_blank" tabindex="-1" rel="external">
                                Wealth 2 You
                            </a>
                        </td>
                    </tr><!-- Email Body -->
                    <tr>
                        <td width="100%" cellpadding="0" cellspacing="0"
                            style="font-family: Avenir, Helvetica, sans-serif;box-sizing: border-box;background-color: #FFFFFF;border-bottom: 1px solid #EDEFF2;border-top: 1px solid #EDEFF2;margin: 0;padding: 0;width: 100%;-premailer-cellpadding: 0;-premailer-cellspacing: 0;-premailer-width: 100%">
                            <table align="center" width="570" cellpadding="0" cellspacing="0"
                                   style="font-family: Avenir, Helvetica, sans-serif;box-sizing: border-box;background-color: #FFFFFF;margin: 0 auto;padding: 0;width: 570px;-premailer-cellpadding: 0;-premailer-cellspacing: 0;-premailer-width: 570px">
                                <!-- Body content -->
                                <tbody>
                                <tr>
                                    <td style="font-family: Avenir, Helvetica, sans-serif;box-sizing: border-box;padding: 35px">
                                        <h1 style="font-family: Avenir, Helvetica, sans-serif;box-sizing: border-box;color: #2F3133;font-size: 19px;font-weight: bold;margin-top: 0;text-align: left">
                                            Hello {{ $recipient }}!</h1>
                                        <p style="font-family: Avenir, Helvetica, sans-serif;box-sizing: border-box;color: #74787E;font-size: 16px;line-height: 1.5em;margin-top: 0;text-align: left">
                                            {{ $payer }} has indicated that they have paid the sum of {{ $amount }} {{ $currency }} into your account.</p>
                                        <p style="font-family: Avenir, Helvetica, sans-serif;box-sizing: border-box;color: #74787E;font-size: 16px;line-height: 1.5em;margin-top: 0;text-align: left">
                                            If you have received this payment, kindly log in to confirm reciept of payment</p>
                                        <table align="center" width="100%" cellpadding="0" cellspacing="0"
                                               style="font-family: Avenir, Helvetica, sans-serif;box-sizing: border-box;margin: 30px auto;padding: 0;text-align: center;width: 100%;-premailer-cellpadding: 0;-premailer-cellspacing: 0;-premailer-width: 100%">
                                            <tbody>
                                            <tr>
                                                <td align="center"
                                                    style="font-family: Avenir, Helvetica, sans-serif;box-sizing: border-box">
                                                    <table width="100%" border="0" cellpadding="0" cellspacing="0"
                                                           style="font-family: Avenir, Helvetica, sans-serif;box-sizing: border-box">
                                                        <tbody>
                                                        <tr>
                                                            <td align="center"
                                                                style="font-family: Avenir, Helvetica, sans-serif;box-sizing: border-box">
                                                                <table border="0" cellpadding="0" cellspacing="0"
                                                                       style="font-family: Avenir, Helvetica, sans-serif;box-sizing: border-box">
                                                                    <tbody>
                                                                    <tr>
                                                                        <td style="font-family: Avenir, Helvetica, sans-serif;box-sizing: border-box">
                                                                            <a href="{{ $link }}"
                                                                               target="_blank"
                                                                               style="font-family: Avenir, Helvetica, sans-serif;box-sizing: border-box;border-radius: 3px;box-shadow: 0 2px 3px rgba(0, 0, 0, 0.16);color: #FFF;display: inline-block;text-decoration: none;-webkit-text-size-adjust: none;background-color: #3097D1;border-top: 10px solid #3097D1;border-right: 18px solid #3097D1;border-bottom: 10px solid #3097D1;border-left: 18px solid #3097D1"
                                                                               tabindex="-1" rel="external">Log in</a>
                                                                        </td>
                                                                    </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                        <p style="font-family: Avenir, Helvetica, sans-serif;box-sizing: border-box;color: #74787E;font-size: 16px;line-height: 1.5em;margin-top: 0;text-align: left">
                                            If you have not recieved the payment, no action is required. But kindly check your account</p>
                                        <!-- Salutation -->
                                        <p style="font-family: Avenir, Helvetica, sans-serif;box-sizing: border-box;color: #74787E;font-size: 16px;line-height: 1.5em;margin-top: 0;text-align: left">
                                            Regards,<br>Wealth 2 You</p>
                                        <!-- Subcopy -->
                                        <table width="100%" cellpadding="0" cellspacing="0"
                                               style="font-family: Avenir, Helvetica, sans-serif;box-sizing: border-box;border-top: 1px solid #EDEFF2;margin-top: 25px;padding-top: 25px">
                                            <tbody>
                                            <tr>
                                                <td style="font-family: Avenir, Helvetica, sans-serif;box-sizing: border-box">
                                                    <p style="font-family: Avenir, Helvetica, sans-serif;box-sizing: border-box;color: #74787E;line-height: 1.5em;margin-top: 0;text-align: left;font-size: 12px">
                                                        If you’re having trouble clicking the "Log in" button,
                                                        copy and paste the URL below
                                                        into your web browser: <a
                                                                href="{{ $link }}"
                                                                style="font-family: Avenir, Helvetica, sans-serif;box-sizing: border-box;color: #3869D4"
                                                                target="_blank" tabindex="-1" rel="external">{{ $link }}</a>
                                                    </p>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td style="font-family: Avenir, Helvetica, sans-serif;box-sizing: border-box">
                            <table align="center" width="570" cellpadding="0" cellspacing="0"
                                   style="font-family: Avenir, Helvetica, sans-serif;box-sizing: border-box;margin: 0 auto;padding: 0;text-align: center;width: 570px;-premailer-cellpadding: 0;-premailer-cellspacing: 0;-premailer-width: 570px">
                                <tbody>
                                <tr>
                                    <td align="center"
                                        style="font-family: Avenir, Helvetica, sans-serif;box-sizing: border-box;padding: 35px">
                                        <p style="font-family: Avenir, Helvetica, sans-serif;box-sizing: border-box;line-height: 1.5em;margin-top: 0;color: #AEAEAE;font-size: 12px;text-align: center">
                                            © {{ date("Y") }} Wealth 2 You. All rights reserved.</p>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        </tbody>
    </table>
</div>