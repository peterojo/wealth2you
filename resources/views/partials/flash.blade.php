@if(session('message'))
    <div class="alert alert-dismissible alert-{{ session('type', 'info') }}">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        {!! session('message') !!}
    </div>
@endif
@if(session('status'))
    <div class="alert alert-dismissible alert-{{ session('type', 'info') }}">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        {!! session('status') !!}
    </div>
@endif