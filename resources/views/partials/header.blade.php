<!--Header Starts-->
<header id="layout_bottom">
    <nav class="navbar navbar-default  bootsnav">
        <div class="container">

            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-menu">
                    <i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand" href="{{ route('welcome') }}"><img src="{{ asset('images/logo.png') }}" class="logo" alt=""></a>
            </div>

            <div class="collapse navbar-collapse" id="navbar-menu">
                <ul class="nav navbar-nav navbar-right" data-in="slideInUp" data-out="fadeOut">
                    <li>
                        <a href="{{ route('welcome') }}" class="dropdown-toggle border" data-toggle="dropdown" >Home</a>

                    </li>
                    <li><a href="{{ route('about') }}" class="border">How It Works</a></li>
                    @if(auth()->check())
                        <li><a href="{{ route('home') }}" class="border">Dashboard</a></li>
                        <li>
                            <a href="{{ route('logout') }}"
                               class="border"
                               onclick="logOut(event);">
                                Log Out
                            </a>
                        </li>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    @else
                        <li><a href="{{ route('register') }}" class="border">Register</a></li>
                        <li><a href="{{ route('login') }}" class="border">Log in</a></li>
                    @endif
                </ul>
            </div>
        </div>
    </nav>
</header>
<!--Header ends-->