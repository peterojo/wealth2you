@extends('layouts.template')

@section('page-title') Contribute @endsection

@section('content')
    <div class="text-center">
        <a href="/" class="logo"><span>Wealth<span>toYou</span></span></a>
        <h5 class="m-t-0 font-600">To start earning cash on the Wealth2You platform, you'll have to
            contribute to other users</h5>
    </div>
    @if(!auth()->user()->registrationFee->isConfirmed())
    <div class="m-t-40 card-box">
	    @include('partials.flash')
	    @if(!auth()->user()->registrationFee->isPaid())
	    <div class="alert alert-dismissable alert-info">
		    Please once payment is made kindly click on the register payment button to register your payment.
		    <div class="clearfix"></div>
		    <button type="button" class="btn btn-info pull-right" data-dismiss="alert" aria-hidden="true">OK, Got it!</button>
		    <div class="clearfix"></div>
	    </div>
	    @endif
	    @if(session('errors'))
		    @foreach($errors->toArray() as $error)
			    <div class="alert alert-dismissible alert-danger">
				    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				    <ul>
					    @foreach($error as $e)
						    <li>{{ $e }}</li>
					    @endforeach
				    </ul>
			    </div>
		    @endforeach
	    @endif
	    @if(request()->has('success'))
		    <div class="alert alert-dismissible alert-success">
			    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			    Payment successful! Register your payment below.
		    </div>
	    @endif
	    @if(request()->has('fail'))
		    <div class="alert alert-dismissible alert-danger">
			    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			    Payment failed! Please try again.
		    </div>
	    @endif
	    @if(auth()->user()->registrationFee->isAwaitingConfirmation())
		    <div class="alert alert-info text-center">
			    Your payment is awaiting confirmation.<br>
			    You will be notified via email when your payment has been confirmed.<br>
			    Thank you for being a part of Wealth 2 You
		    </div>
	    @endif
	    <div class="text-center">
		    <h4 class="text-uppercase font-bold m-b-0">Registration Fee</h4>
	    </div>
	    <div class="panel-body">
		    <div class="inbox-widget nicescroll">
			    {{--<form action="https://voguepay.com/pay/" method="post">
				    <div class="inbox-item">
					    <input type="hidden" name="v_merchant_id" value="14130-18518">
					    <input type="hidden" name="cur" value="{{ auth()->user()->registrationFee->currency }}">
					    <input type="hidden" name="merchant_ref" value="{{ auth()->user()->registrationFee->id }}">
					    <input type="hidden" name="memo" value="{{ "Wealth 2 You registration fee: ".auth()->user()->name }}">
					    <input type="hidden" name="total" value="{{ auth()->user()->registrationFee->amount }}">
					    <input type="hidden" name="developer_code" value="594daad6e6167">
					    <input type="hidden" name="notify_url" value="{{ route('vogue-webhook') }}">
					    <input type="hidden" name="success_url" value="{{ url()->current() }}?success">
					    <input type="hidden" name="fail_url" value="{{ url()->current() }}?fail">

					    <input type='image' src='http://voguepay.com/images/buttons/make_payment_blue.png' alt='Submit' class="pull-right" />
				    </div>
			    </form>--}}
			    <form action="{{ route('activate.post') }}" method="post">
				    <div class="inbox-item">
					    <div class="inbox-item-img"><img src="{{ asset('assets/images/users/avatar-5.jpg') }}" class="img-circle"
					                                     alt=""></div>
					    <p class="inbox-item-author">Greater Grace</p>
					    <p class="inbox-item-text">Diamond Bank 0078517658</p>
				    </div>
				    <div class="inbox-item">
					    <p class="header-title text-center">{{ auth()->user()->registrationFee->amount }} {{ auth()->user()->registrationFee->currency }}</p>
				    </div>
				    @if(auth()->user()->registrationFee->isConfirmed())
					    <div class="alert alert-success text-center">Confirmed</div>
				    @elseif(auth()->user()->registrationFee->isAwaitingConfirmation())
					    <div class="alert alert-warning text-center">Awaiting Confirmation</div>
				    @else
					    <div class="inbox-item">
						    {{ csrf_field() }}
						    <input type="hidden" name="feeId" value="{{ auth()->user()->registrationFee->id }}">
						    <button type="submit" class="btn btn-primary btn-block">Register Payment</button>
					    </div>
				    @endif
			    </form>
		    </div>
	    </div>

    </div>
    @endif
    <div class="m-t-40 card-box">
	    @if(auth()->user()->registrationFee->isConfirmed())
        @include('partials.flash')
	    @endif
        <div class="text-center">
            @if(auth()->user()->hasCertificates())
                <div class="alert">
                    All your contributions have been confirmed.<br>
                    <a href="{{ route('invite') }}" class="btn btn-block btn-info">Proceed</a>
                </div>
            @else
                <h4 class="text-uppercase font-bold m-b-0">Contribute To These Accounts</h4>
            @endif
        </div>
        <div class="panel-body">
            <div class="inbox-widget nicescroll" style="height: auto;">
                @forelse($transactions as $transaction)
                    <div class="inbox-item">
                        <div class="inbox-item-img"><img src="{{ asset('assets/images/users/avatar-1.jpg') }}" class="img-circle"
                                                         alt=""></div>
                        <p class="inbox-item-author">{{ $transaction->recipient->account->name }} <span class="label label-inverse pull-right">{{ $transaction->amount . " " . $transaction->currency }}</span></p>
                        <p class="inbox-item-text">{{ $transaction->recipient->account->bank . ": " . $transaction->recipient->account->number }}</p>
                        <p class="inbox-item-text text-right"><i class="fa fa-phone-square"></i>&nbsp; {{ $transaction->recipient->phone }}</p>
                        @if($transaction->isConfirmed())
                            <div class="alert alert-success text-center">Confirmed</div>
                        @elseif($transaction->isAwaitingConfirmation())
                            <div class="alert alert-warning text-center">Awaiting Confirmation</div>
                        @else
		                    <a class="btn btn-primary btn-block" data-toggle="modal" href="#_{{ $transaction->trans_id }}">
			                    Register Payment
		                    </a>
		                    <div class="modal fade" id="_{{ $transaction->trans_id }}">
			                    <div class="modal-dialog">
				                    <div class="modal-content">
					                    <div class="modal-header">
						                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
							                    &times;
						                    </button>
						                    <h4 class="modal-title">{{ $transaction->currency." ".$transaction->amount }} to {{ $transaction->recipient->name }}</h4>
					                    </div>

					                    <form action="{{ route('contribute.post', $transaction->trans_id) }}" method="post" enctype="multipart/form-data">
						                    <div class="modal-body">
							                    <div class="form-group">
								                    <label for="upload">Upload Proof of Payment</label>
								                    <input type="file" name="upload" class="form-control">
							                    </div>
						                    </div>
						                    <div class="modal-footer">
							                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close
							                    </button>
							                    {{ csrf_field() }}
							                    <button type="submit" class="btn btn-primary">Finish Payment Registration</button>
						                    </div>
					                    </form>

				                    </div><!-- /.modal-content -->
			                    </div><!-- /.modal-dialog -->
		                    </div><!-- /.modal -->
                        @endif
                    </div>
                @empty
                    <div class="inbox-item">
                        <div class="inbox-item-img"><img src="{{ asset('assets/images/users/avatar-5.jpg') }}" class="img-circle"
                                                         alt=""></div>
                        <p class="inbox-item-author">There are no accounts to uplines to pay at the moment. Please check back later.</p>
                        <p class="inbox-item-text">Thank you!</p>
                    </div>
                @endforelse
            </div>
        </div>

    </div>
    <div class="row">
        <div class="col-sm-12 text-center">
            <p class="text-muted">If you're having any trouble, <a data-toggle="modal" href="#contact-support" class="text-primary m-l-5"><b>Contact Support</b></a></p>
        </div>
    </div>
    <div class="modal fade" id="contact-support">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Contact Wealth2You Customer Support</h4>
                </div>
                <form action="{{ route('ticket.store') }}" method="post" role="form">
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="trans_id">Transaction</label>
                            <select name="trans_id" id="trans_id" class="form-control" required>
                                <option value="">Select</option>
                                @foreach($transactions as $transaction)
                                    <option value="{{ $transaction->trans_id }}">{{ $transaction->currency." ".number_format($transaction->amount)." to ".$transaction->recipient->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="subject">Subject</label>
                            <input type="text" class="form-control" name="subject" id="subject" required>
                        </div>
                        <div class="form-group">
                            <label for="body">Message</label>
                            <textarea name="message" id="body" class="form-control" required></textarea>
                        </div>
                    </div>
                    <div class="modal-footer">
                        {{ csrf_field() }}
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Send</button>
                    </div>
                </form>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
@endsection