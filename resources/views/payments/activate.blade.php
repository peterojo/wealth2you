@extends('layouts.template')

@section('page-title') Registration Fee Payment @endsection

@section('content')
    <div class="text-center">
        <a href="/" class="logo"><span>Wealth<span>toYou</span></span></a>
        <h5 class="m-t-0 font-600">To start earning cash on the Wealth2You platform, you'll have to
            contribute to other users</h5>
    </div>
    <div class="m-t-40 card-box">
	    @include('partials.flash')
	    <div class="alert alert-dismissable alert-info">
		    Please once payment is made kindly click on the register payment button to register your payment.
		    <div class="clearfix"></div>
		    <button type="button" class="btn btn-info pull-right" data-dismiss="alert" aria-hidden="true">OK, Got it!</button>
		    <div class="clearfix"></div>
	    </div>
        @if(session('errors'))
            @foreach($errors->toArray() as $error)
                <div class="alert alert-dismissible alert-danger">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <ul>
                        @foreach($error as $e)
                            <li>{{ $e }}</li>
                        @endforeach
                    </ul>
                </div>
            @endforeach
        @endif
        @if(request()->has('success'))
            <div class="alert alert-dismissible alert-success">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                Payment successful! Register your payment below.
            </div>
        @endif
        @if(request()->has('fail'))
            <div class="alert alert-dismissible alert-danger">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                Payment failed! Please try again.
            </div>
        @endif
        @if(auth()->user()->registrationFee->isAwaitingConfirmation())
            <div class="alert alert-info text-center">
                Your payment is awaiting confirmation.<br>
                You will be notified via email when your payment has been confirmed.<br>
                Thank you for being a part of Wealth 2 You
            </div>
        @endif
        <div class="text-center">
            <h4 class="text-uppercase font-bold m-b-0">Registration Fee</h4>
        </div>
        <div class="panel-body">
            <div class="inbox-widget nicescroll">
                <form action="https://voguepay.com/pay/" method="post">
                    <div class="inbox-item">
                        <input type="hidden" name="v_merchant_id" value="14130-18518">
                        <input type="hidden" name="cur" value="{{ auth()->user()->registrationFee->currency }}">
                        <input type="hidden" name="merchant_ref" value="{{ auth()->user()->registrationFee->id }}">
                        <input type="hidden" name="memo" value="{{ "Wealth 2 You registration fee: ".auth()->user()->name }}">
                        <input type="hidden" name="total" value="{{ auth()->user()->registrationFee->amount }}">
                        <input type="hidden" name="developer_code" value="594daad6e6167">
                        <input type="hidden" name="notify_url" value="{{ route('vogue-webhook') }}">
                        <input type="hidden" name="success_url" value="{{ url()->current() }}?success">
                        <input type="hidden" name="fail_url" value="{{ url()->current() }}?fail">

                        <input type='image' src='http://voguepay.com/images/buttons/make_payment_blue.png' alt='Submit' class="pull-right" />
                    </div>
                </form>
                <form action="{{ route('activate.post') }}" method="post">
                    <div class="inbox-item">
                        <div class="inbox-item-img"><img src="{{ asset('assets/images/users/avatar-5.jpg') }}" class="img-circle"
                                                         alt=""></div>
                        <p class="inbox-item-author">Greater Grace</p>
                        <p class="inbox-item-text">Diamond Bank 0078517658</p>
                    </div>
                    <div class="inbox-item">
                        <p class="header-title text-center">{{ auth()->user()->registrationFee->amount }} {{ auth()->user()->registrationFee->currency }}</p>
                    </div>
	                @if(auth()->user()->registrationFee->isConfirmed())
		                <div class="alert alert-success text-center">Confirmed</div>
	                @elseif(auth()->user()->registrationFee->isAwaitingConfirmation())
		                <div class="alert alert-warning text-center">Awaiting Confirmation</div>
	                @else
                    <div class="inbox-item">
                        {{ csrf_field() }}
                        <input type="hidden" name="feeId" value="{{ auth()->user()->registrationFee->id }}">
                        <button type="submit" class="btn btn-primary btn-block">Register Payment</button>
                    </div>
					@endif
                </form>
            </div>
        </div>

    </div>
@endsection