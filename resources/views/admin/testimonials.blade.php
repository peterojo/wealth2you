@extends('dashboard.master')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                @include('partials.flash')
                <div class="card-box">
                    <h4 class="header-title m-t-0 m-b-30">User Testimonials</h4>

                    <p class="text-muted font-13 m-b-25">

                    </p>

                    <div class="table-responsive">
                        <table class="table m-0">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Title</th>
                                <th>Body</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @php $i=1; @endphp
                            @forelse($testimonials as $testimonial)
                            <tr>
                                <th scope="row">{{ $i++ }}</th>
                                <td>{{ $testimonial->user->name }}</td>
                                <td>{{ $testimonial->title }}</td>
                                <td>{{ $testimonial->body }}</td>
                                <td>
                                    @if($testimonial->enabled)
                                    <span class="label label-success" style="padding: 5px 20px;">Enabled</span>
                                    @else
                                    <span class="label label-danger" style="padding: 5px 20px;">Disabled</span>
                                    @endif
                                </td>
                                <td>
                                    @if($testimonial->enabled)
                                        <form action="{{ route('admin.disable-testimonial', $testimonial->id) }}" method="post">
                                            {{ csrf_field() }}
                                            <button type="submit" class="btn btn-danger waves-effect">
                                                Disable
                                            </button>
                                        </form>
                                    @else
                                        <form action="{{ route('admin.enable-testimonial', $testimonial->id) }}" method="post">
                                            {{ csrf_field() }}
                                            <button type="submit" class="btn btn-success waves-effect">
                                                Enable
                                            </button>
                                        </form>
                                    @endif
                                </td>
                            </tr>
                            @empty
                                <tr>
                                    <td colspan="6" style="text-align: center">No testimonials</td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>
                        {{ $testimonials->links() }}
                    </div>
                </div>
            </div><!-- end col -->

        </div>
        <!-- end row -->
    </div> <!-- container -->
@endsection