@extends('dashboard.master')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                @include('partials.flash')
                <div class="card-box">
                    <h4 class="header-title m-t-0 m-b-30">User Registration Fee Payments</h4>

                    <p class="text-muted font-13 m-b-25">

                    </p>

                    <div class="table-responsive">
                        <table class="table m-0">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Phone Number</th>
	                            <th>Amount</th>
                                <th>Payment Status</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @php $i=1; @endphp
                            @forelse($registrations as $fee)
                            <tr>
                                <th scope="row">{{ $i++ }}</th>
                                <td>{{ $fee->user->name }}</td>
                                <td>{{ $fee->user->email }}</td>
                                <td>{{ $fee->user->phone }}</td>
	                            <td>{{ $fee->currency." ".$fee->amount }}</td>
                                <td>
                                    @if($fee->isConfirmed())
                                    <span class="label label-success" style="padding: 5px 20px;">Confirmed</span>
                                    @elseif($fee->isAwaitingConfirmation())
                                    <span class="label label-warning" style="padding: 5px 20px;">Awaiting Confirmation</span>
                                    @else
                                    <span class="label label-danger" style="padding: 5px 20px;">Unpaid</span>
                                    @endif
                                </td>
                                <td>
                                    @if($fee->isAwaitingConfirmation())
                                        <form action="{{ route('admin.confirm-registration') }}" method="post">
                                            {{ csrf_field() }}
                                            <input type="hidden" name="feeId" value="{{ $fee->id }}">
                                            <button type="submit" class="btn btn-info waves-effect">
                                                Confirm
                                            </button>
                                        </form>
                                        <form action="{{ route('admin.deny-registration', $fee->id) }}" method="post">
                                            {{ csrf_field() }}
                                            {{--<input type="hidden" name="feeId" value="{{ $fee->id }}">--}}
                                            <button type="submit" class="btn btn-danger waves-effect">
                                                Deny
                                            </button>
                                        </form>
                                    @endif
                                </td>
                            </tr>
                            @empty
                                <tr>
                                    <td colspan="6" style="text-align: center">No payments</td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>
                        {{ $registrations->links() }}
                    </div>
                </div>
            </div><!-- end col -->

        </div>
        <!-- end row -->
    </div> <!-- container -->
@endsection