@extends('master')

@section('content')
    <!--ERROR-->
    <section id="error" class="text-center padding-bottom-half padding-top">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <img src="{{ asset('images/404.png') }}" alt="404" class="error_inner img-responsive">
                    <div class="content">
                        <h2> 404 Error</h2>
                        <p>The page you are looking for does not exist.</p>
                    </div>
                </div>
                <a href="{{ route('home') }}" class="btn-green text-uppercase border_radius top40">go back to home page</a>
            </div>
        </div>
    </section>
    <!--ERROR ends-->
@stop