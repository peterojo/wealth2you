@extends('layouts.template')

@section('page-title') Register Bank Account @endsection

@section('content')
    <div class="text-center">
        <a href="/" class="logo"><span>Wealth<span>toYou</span></span></a>
        <h5 class="text-muted m-t-0 font-600">Register your bank account information</h5>
    </div>
    <div class="m-t-40 card-box">
        <div class="text-center">
            <h4 class="text-uppercase font-bold m-b-0">Your Bank Account</h4>
        </div>
        <div class="panel-body">
            @if (session('status'))
                <div class="alert alert-{{ session('type', 'danger') }}">
                    {{ session('status') }}
                </div>
            @endif
            <form class="form-horizontal m-t-20" action="{{ route('store.account') }}" method="post">
                {{ csrf_field() }}
                <div class="form-group{{ $errors->has('bank_name') ? ' has-error' : '' }}">
                    <div class="col-xs-12">
                        <input class="form-control" name="bank_name" type="text" required placeholder="Name of Bank" value="{{ old('bank_name') }}">
                    </div>
                </div>
                @if ($errors->has('bank_name'))
                    <div class="alert alert-danger alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        {{ $errors->first('bank_name') }}
                    </div>
                @endif
                <div class="form-group{{ $errors->has('account_number') ? ' has-error' : '' }}">
                    <div class="col-xs-12">
                        <input class="form-control" type="number" name="account_number" required placeholder="Account Number" value="{{ old('account_number') }}">
                    </div>
                </div>
                @if ($errors->has('account_number'))
                    <div class="alert alert-danger alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        {{ $errors->first('account_number') }}
                    </div>
                @endif
                <div class="form-group{{ $errors->has('account_holder') ? ' has-error' : '' }}">
                    <div class="col-xs-12">
                        <input class="form-control" type="text" name="account_holder" required placeholder="Account Holder Name" value="{{ old('account_holder') }}">
                    </div>
                </div>
                @if ($errors->has('account_holder'))
                    <div class="alert alert-danger alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        {{ $errors->first('account_holder') }}
                    </div>
                @endif
                <div class="form-group text-center m-t-30">
                    <div class="col-xs-12">
                        <button class="btn btn-custom btn-bordred btn-block waves-effect waves-light" type="submit">Save &amp; Proceed</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <!-- end card-box-->
@endsection