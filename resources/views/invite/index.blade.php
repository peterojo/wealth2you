@extends('layouts.template')

@section('page-title') Invite Contacts @endsection

@section('styles')
    @parent
    <style>
        .fb, .fb:hover, .fb:active, .fb:focus {
            background-color: #3b5998;
            color: #FFFFFF;
        }
        .contact-row {
            border-bottom: 1px #d4d4d4 solid;
            clear: both;
            padding-bottom: 10px;
        }
        .remove-row {
            border-radius: 48% !important;
        }
    </style>
@endsection

@section('opinionated-script')
    <script>
        window.fbAsyncInit = function() {
            FB.init({
                appId      : '1904456086503750',
                xfbml      : true,
                version    : 'v2.9'
            });
            FB.AppEvents.logPageView();
        };

        (function(d, s, id){
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) {return;}
            js = d.createElement(s); js.id = id;
            js.src = "//connect.facebook.net/en_US/sdk.js";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>
@endsection

@section('content')
    <div class="text-center">
        <a href="{{ route('welcome') }}" class="logo"><span>Wealth<span>toYou</span></span></a>
        <h5 class="m-t-0 font-600">You can start inviting your friends right away!</h5>
    </div>
    <div class="m-t-40 card-box">
        @include('partials.flash')
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="text-center">
            <h4 class="text-uppercase font-bold m-b-0">Invite your contacts</h4>
        </div>
        <div class="panel-body">
            <div class="inbox-widget nicescroll">
                <div class="inbox-item">
                    <button type="button" onclick="sendFbInvite();" class="btn btn-lg btn-block fb"><i class="fa fa-facebook-square"></i>&nbsp;&nbsp;Send Facebook Invite</button>
                </div>
	            <div class="alert alert-info">
		            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		            <strong>Share on Facebook</strong><br> You can also copy and paste your registration link from your certificate and share it on your facebook wall.
	            </div>
            </div>
        </div>
        <div class="panel-body">
            <div class="inbox-widget nicescroll">
                <div class="inbox-item">
                    <a data-toggle="modal" href="#add-invites" class="btn btn-default btn-lg btn-block"><i class="fa fa-plus-square"></i>&nbsp;&nbsp;Invite Contacts</a>
                </div>
            </div>
        </div>
        <div class="panel-body">
            <div class="inbox-widget nicescroll">
                <div class="inbox-item">
                    <a href="{{ route('home') }}" class="btn btn-primary pull-right">Proceed to Dashboard&nbsp;&xrarr;</a>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="add-invites">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Add Contacts</h4>
                </div>
                <form action="{{ route('invite.store') }}" method="post" role="form" class="form">
                    <div class="modal-body row">

                        <div class="contact-row">
                            <div class="form-group col-sm-4">
                                <label for="name">Name</label><br>
                                <input type="text" class="form-control" name="name[]" required>
                            </div>
                            <div class="form-group col-sm-4">
                                <label for="email">Email</label><br>
                                <input type="email" name="email[]" class="form-control" required>
                            </div>
                            <div class="form-group col-sm-4">
                                <label for="phone">Phone</label><br>
                                <input type="text" name="phone[]" class="form-control" required>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <button class="btn btn-primary pull-right" id="add-row"><i class="fa fa-plus-circle"></i> Add</button>
                    <div class="clearfix"></div>
	                <button class="btn btn-success pull-right" id="add-body"><i class="fa fa-pencil-square"></i> Add Custom Message</button>
	                <div class="clearfix"></div>
                    <div class="modal-footer">
                        {{ csrf_field() }}
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Send</button>
                    </div>
                </form>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
@endsection

@section('scripts')
    @parent
    <script>
        function sendFbInvite () {
            FB.ui({
                method: 'send',
                link: '{{ url()->route('register') }}?ref={{ auth()->user()->availableCertificate()->code }}',
                redirect_uri: '{{ url()->current() }}'
            });
        }
        $('#add-row').on('click', function(e) {
            e.preventDefault();
            $('#remove-row').show();
            $('.modal-body').append(
                '<div class="contact-row">' +
                    '<div class="form-group col-sm-4">' +
                        '<label for="name">Name</label><br>' +
                        '<input type="text" class="form-control" name="name[]" required>' +
                    '</div>' +
                    '<div class="form-group col-sm-4">' +
                        '<label for="email">Email</label><br>' +
                        '<input type="email" name="email[]" class="form-control" required>' +
                    '</div>' +
                    '<div class="form-group col-sm-4">' +
                        '<label for="phone">Phone</label><br>' +
                        '<input type="text" name="phone[]" class="form-control" required>' +
                    '</div>' +
                    '<button class="btn btn-sm btn-danger pull-right remove-row"><i class="fa fa-minus"></i></button>' +
                    '<div class="clearfix"></div>' +
                '</div>'
            );
            $('.remove-row').on('click', function (e) {
                e.preventDefault();
                $(this).parent().remove();
            });
        });
        $('#add-body').on('click', function(e) {
            e.preventDefault();
            $('.modal-body').prepend(
                '<div class="contact-row add-body-block">\
                <div class="col-sm-12 form-group">\
                <label for="emailBody">Message</label>\
                <textarea name="emailBody" id="emailBody" class="form-control"></textarea>\
                </div>\
                </div>'
            );
            $('#emailBody').focus();
        });
    </script>
@endsection